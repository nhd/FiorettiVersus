package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public class StagesScreen implements Screen{
	final ProjectF game;
	private Music bgm;
	private Texture icon;
	public static TextureRegion[] stage = new TextureRegion[12]; //27
	private Texture stages;
	
	private String[] trackName = {"    RANDOM","NEET Samurai", " Super Warrior","   Soul Reaper","    North Star","Killer Teacher","   Pirate King",
			"  Orange Ninja","Armored Soldier","Spirit Detective",
	};
		
	public StagesScreen(ProjectF game){
		this.game = game;
		stages = game.getManager().get(AssetConstant.STAGE_SEL_STAGES, Texture.class);
		bgm = game.getManager().get(AssetConstant.STAGE_SEL_BG_MUSIC, Music.class);
		icon = game.getManager().get(AssetConstant.STAGE_SEL_MUSIC_ICON, Texture.class);
		
		if(game.getBattleSettings().getMode()==Constant.TOURNAMENT_MODE){
			stage[0] = new TextureRegion(stages,1280,0,640,360);
		}else{
			if (game.getBattleSettings().isVsCpu())
				stage[0] = new TextureRegion(stages,640,0,640,360);
			else
				stage[0] = new TextureRegion(stages,0,0,640,360);
		}

		for (int i=0;i<(stage.length-2)/5;i++){
			for (int j=1;j<6;j++){
				stage[(5*i)+j] = new TextureRegion(stages,640*i,360*j,640,360);
			}
		}
		if (game.getSettings().isRandomStage() && !game.getSettings().isStageReady())
			game.getSettings().setStage(stage.length-1);
	}
	
	public void render(float delta){
		game.batch.begin();
			draw();
		game.batch.end();
		keyInputs();
	}
	
	private void draw(){
		game.batch.draw(stage[0], 0, 0);
		if(game.getSettings().getStage()!=stage.length-1)
			game.batch.draw(stage[game.getSettings().getStage()], 160, 90, 320, 180);
//		game.font.draw(game.batch, stageName[game.getSettings().stage], 160, 290);
//		game.batch.draw(icon, 240, 35);
		game.font.setColor(Color.WHITE);
//		game.font.draw(game.batch, trackName[NewSettings.getTrack()], 270, 50);
	}
	
	private void keyInputs(){
		if(game.getInputs().getStageInput(stage.length)){
			bgm.stop();
			dispose();
		}
	}
	
	@Override
	public void resize(int width, int height){}
	@Override
	public void show(){
		bgm.setVolume(NewSettings.getMastervol());
		bgm.setLooping(true);
		bgm.play();
	}
	@Override
	public void hide(){}
	@Override
	public void pause(){}
	@Override
	public void resume(){}
	@Override
	public void dispose(){}

}
