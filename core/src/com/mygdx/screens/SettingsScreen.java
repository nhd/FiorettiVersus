package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.fighters.CpuModule;
import com.mygdx.model.AssetConstant;
//calibri 18
public class SettingsScreen implements Screen{
	final ProjectF game;
	private Texture background;
	private Texture cursor;
	private int selectedRow = 275;
	private String[] cpuBehav = {"cautious","aggressive"};

	public SettingsScreen(ProjectF game){
		this.game = game;
		background = game.getManager().get(AssetConstant.SETTINGS_BG, Texture.class);
		cursor = game.getManager().get(AssetConstant.SETTINGS_CURSOR, Texture.class);
	}
	
	public void render(float delta){
		display();
		selectedRow = game.getInputs().getSettingsInputs(selectedRow);
	}
	
	private void display(){
		String volume = NewSettings.getMastervolString();
		game.batch.begin();
			game.batch.draw(background,0,0);
			game.batch.draw(cursor, 53, selectedRow);
			game.font.draw(game.batch, volume, 250, 285);
			game.font.draw(game.batch,cpuBehav[CpuModule.aggressivity],250,258);
		game.batch.end();
	}

	@Override
	public void resize(int width, int height){}
	@Override
	public void show(){}
	@Override
	public void hide(){}
	@Override
	public void pause(){}
	@Override
	public void resume(){}
	@Override
	public void dispose(){}
	
}
