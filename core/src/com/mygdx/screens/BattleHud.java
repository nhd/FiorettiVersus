package com.mygdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.BattleSettings;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public class BattleHud {
	final ProjectF game;
	private BattleSettings settings;
	
	private int[] order = {1,2,1};
	private int priority = 0;

	private Vector2 v2 = new Vector2();

	private int scaleB;
	private int scaleR;

	private final Color redhit = new Color(1,0,0,0.2f);
	private final  Color staminaBar = new Color(1,0.87f,0,1.0f);
	
	private BitmapFont myfont = new BitmapFont();
	private GlyphLayout blueNameLayout = new GlyphLayout();
	
	private Texture huds;
	private Texture tournament;
	private TextureRegion[] hud = new TextureRegion[2];
	private TextureRegion stage;
	
	private int intro = 0;
	private float readypos = 190;
	private String winner = "";
	
	private final float yratio = Gdx.graphics.getHeight()/Constant.WINDOW_HEIGHT;
	private final float xratio = Gdx.graphics.getWidth()/Constant.WINDOW_WIDTH;
	
	private static boolean shake = false;
	private ShapeRenderer sr = new ShapeRenderer();
	
	public BattleHud(ProjectF game, Fighter red, Fighter blue){//, ShapeRenderer sr){
		this.game = game;
		huds = game.getManager().get(AssetConstant.BATTLE_HUD, Texture.class);
		this.settings = game.getBattleSettings();
		if(game.getSettings().getMode()==Constant.TOURNAMENT_MODE){
			tournament = game.getManager().get(AssetConstant.STAGE_TOURNAMENT, Texture.class);
			stage = new TextureRegion(tournament);
		}else
			stage = StagesScreen.stage[game.getSettings().getStage()];
		
		hud[0] = new TextureRegion(huds,0,0,640,120);
		hud[1] = new TextureRegion(huds,0,120,640,120);
		
		scaleB = blue.getMaxHealth()/10;
		scaleR = red.getMaxHealth()/10;
		
		myfont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		myfont.getData().setScale(2);
	}
	
	 void shake(OrthographicCamera cam, float intensity){
		if (shake){
			cam.translate(MathUtils.random(-intensity,intensity), MathUtils.random(-intensity,intensity));
		}	
		else if(cam.position.x!=320 && cam.position.y!=180){
			cam.position.set(320,180,0);
		}
	}
	
	public void dispose(){}
	
	public void draw(Fighter red, Fighter blue){
		game.batch.begin();
			game.batch.draw(stage,0,0);
			drawFighters(red, blue);
			drawHud();	
			drawNames(red.getName(), blue.getName());		
			drawChain(red.getCombo(), blue.getCombo());
		game.batch.end();
	}
	
	public void drawShapes(Fighter red, Fighter blue){
		sr.begin(ShapeType.Filled);
			drawChainTime(red, blue);
			drawHpBars(red.getHealth(), blue.getHealth());
			drawWins(red.getVictory(), blue.getVictory());
			drawStaminaBars(red.getEnergy(), blue.getEnergy());
			drawHurt(red, blue);
		sr.end();
	}
	
	public String readyGo(long start){
		String phase = "ready";
		darkRect();		
		game.batch.begin();
			game.font.setColor(Color.WHITE);
			String msg = "READY ??";
			if(intro==1)
				msg = "FOULQUE !!!";
			game.font.draw(game.batch, msg, 280, readypos);
		game.batch.end();
		if (TimeUtils.millis() - start >600*(1+intro)) {
			if(intro==0){
				Sounds.getGosound().play(NewSettings.getMastervol()*2);
				intro=1;
			}else{
				phase = "play";
				intro=0;
			}			
		}
		return phase;
	}
	
	private void darkRect(){
		sr.begin(ShapeType.Filled);
			sr.setColor(new Color(0,0,0,0.85f));		
			sr.rect(0, (readypos-15)*yratio, Constant.WINDOW_WIDTH*xratio, 20*yratio);
		sr.end();	
	}
	
	private void drawFighters(Fighter red, Fighter blue){
		if (red.getStance().equals("atk") && !blue.getStance().equals("atk")) {
			priority=1;
		}else if (!red.getStance().equals("atk") && blue.getStance().equals("atk")){
			priority=0;
		}else{
			priority = 0;
		}
		drawFighters(red,blue,order[priority]);
		drawFighters(red,blue,order[priority+1]);
	}
	
	private void drawHud(){
		if (game.getBattleSettings().isVsCpu())
			game.batch.draw(hud[1],0,240);
		else
			game.batch.draw(hud[0],0,240);
	}
	
	private void drawNames(String redname, String bluename){
		game.font.setColor(Color.WHITE);
		game.font.draw(game.batch, redname.toUpperCase(), 52, 353);
		blueNameLayout.setText(game.font, bluename.toUpperCase());		
		game.font.draw(game.batch, blueNameLayout, 588-blueNameLayout.width, 353);
	}

	private void drawChain(int redCombo, int blueCombo){
		game.font.setColor(Color.ORANGE);
		game.font.getData().setScale(2);
		String redstr = "CHAIN : "+Integer.toString(redCombo);
		String bluestr = "CHAIN : "+Integer.toString(blueCombo);
		if (redCombo>0)
			game.font.draw(game.batch, redstr, 30, 290);
		if (blueCombo>0)
			game.font.draw(game.batch, bluestr, 485, 290);
		game.font.getData().setScale(1);
	}

	private void drawFighters(Fighter red, Fighter blue, int num){
		Sprite redp = red.getSprite(blue.getCenter(v2));
		Sprite bluep = blue.getSprite(red.getCenter(v2));
		
		if (red.getHealth()>0 && num==1){
			game.batch.draw(redp, red.getX(), red.getY());
			
		}else if (blue.getHealth()>0 && num==2){
			game.batch.draw(bluep, blue.getX(), blue.getY());
		}

		if(blue.getSpecialAttack().isActive())		
			game.batch.draw(blue.getSpecialAttack().getSprite(), blue.getSpecialAttack().getX(), blue.getSpecialAttack().getY());
		
		if(red.getSpecialAttack().isActive())
			game.batch.draw(red.getSpecialAttack().getSprite(), red.getSpecialAttack().getX(), red.getSpecialAttack().getY());
	}
	
	private void drawHpBars(int redHealth, int blueHealth){
		float bluehp = (240-(blueHealth*24/scaleB))*xratio;
		float redhp = (-240+(redHealth*24/scaleR))*xratio;
		sr.setColor(Color.BLACK);
		sr.rect(346*xratio, 316*yratio, bluehp, 20*yratio);
		sr.rect(294*xratio, 316*yratio, redhp, 20*yratio);
	}
	
	private void drawChainTime(Fighter red, Fighter blue){
		float redComboTimer=red.getRecoveryCombo()/red.getComboTimer()*xratio*100;
		float blueComboTimer=-blue.getRecoveryCombo()/blue.getComboTimer()*xratio*100;
		if (red.getCombo()>0){
			sr.setColor(Color.ORANGE);
			sr.rect(20*xratio, 257*yratio, redComboTimer, 3*yratio);
		}
		if (blue.getCombo()>0){
			sr.setColor(Color.ORANGE);
			sr.rect(626*xratio, 257*yratio, blueComboTimer, 3*yratio);
		}		
	}
	
	private void drawStaminaBars(double redEnergy, double blueEnergy){
		float redStam = 2*(float)redEnergy*xratio;
		float blueStam = -2*(float)blueEnergy*xratio;
		sr.setColor(staminaBar);
		sr.rect(53*xratio, 308*yratio, redStam, 5*yratio);
		sr.rect(588*xratio, 308*yratio,blueStam, 5*yratio);
	}
	
	private void drawHurt(Fighter red, Fighter blue){
		sr.setColor(redhit);
		if (red.getRecoveryHit()>0 && red.getHealth()>0){				
			sr.rect(red.x*xratio, red.y*yratio, red.getWidth()*xratio, red.getHeight()*yratio);
		}
		if (blue.getRecoveryHit()>0 && blue.getHealth()>0){
			sr.rect(blue.x*xratio, blue.y*yratio, blue.getWidth()*xratio, blue.getHeight()*yratio);				
		}
	}
	
	private void drawWins(int redVictory, int blueVictory){
		if (redVictory>0){
			if (game.getBattleSettings().isVsCpu())
				sr.setColor(Color.GRAY);
			else
				sr.setColor(Color.RED);
			for (int i=0;i<redVictory;i++)
				sr.circle((285-20*i)*xratio, 347*yratio, 5*xratio);
		}
		if (blueVictory>0){
			sr.setColor(Color.BLUE);
			
			for (int i=0;i<blueVictory;i++)
				sr.circle((355+20*i)*xratio, 347*yratio, 5*xratio);
		}

	}
	
	public String result(Fighter red, Fighter blue){
		Sounds.getEndsound().play(NewSettings.getMastervol()*2);
		BattleHud.setShake(false);
		if (blue.getHealth()>0 && red.getHealth()<=0){
			blue.setVictory(blue.getVictory()+1);
			winner = blue.getName().toUpperCase()+" WINS";
			myfont.setColor(Color.BLUE);
		}
		else if (red.getHealth()>0 && blue.getHealth()<=0){
			red.setVictory(red.getVictory()+1);
			winner = red.getName().toUpperCase()+" WINS";
			if (game.getBattleSettings().isVsCpu())
				myfont.setColor(Color.GRAY);
			else
				myfont.setColor(Color.RED);
		}else{
			winner = "DOUBLE K.O.";
			myfont.setColor(Color.BLACK);
		}
		return "next";
	
	}
	
	public void endBattleMessage(){
		sr.begin(ShapeType.Filled);
			sr.setColor(Color.WHITE);
			sr.rect(0, 160*yratio, Constant.WINDOW_WIDTH*xratio, 35*yratio);
		sr.end();
		game.batch.begin();		
			GlyphLayout announce = new GlyphLayout();
			announce.setText(myfont, winner);
//			myfont.getData().setScale(2);
			myfont.draw(game.batch,announce,320-(announce.width)/2,190);
	//		if (blue.getHp()==blue.getMaxHp() || red.getHp()==red.getMaxHp()){
	//			myfont.draw(game.batch,"PERFECT",250,165);
	//		}
		game.batch.end();	
	}

	public static boolean isShake() {
		return shake;
	}

	public static void setShake(boolean shake) {
		BattleHud.shake = shake;
	}

}
