package com.mygdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.mygdx.game.ProjectF;
import com.mygdx.model.DatabaseManager;

public class StartupScreen implements Screen{
	private final ProjectF game;
	private Texture loading = new Texture(Gdx.files.internal("graphics/loading.png"));
	private ShapeRenderer sr = new ShapeRenderer();
	float progress = 0;
	
	public StartupScreen(ProjectF game) {
		this.game = game;
	}
	
	@Override
	public void show() {}

	@Override
	public void render(float delta){
		game.batch.begin();
			game.batch.draw(loading, 0, 0);
		game.batch.end();
		drawLoadingBar();
		if(game.isLoadingFinished() && DatabaseManager.firstTimeConnect()){
			game.setScreen(new TitleScreen(game));
		}
	}
	
	private void drawLoadingBar(){
		progress = game.getManager().getProgress();
		sr.begin(ShapeType.Filled);
			sr.rect(40, 27, progress*400, 20);
		sr.end();
		sr.begin(ShapeType.Line);
			sr.rect(40, 27, 400, 20);
		sr.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
