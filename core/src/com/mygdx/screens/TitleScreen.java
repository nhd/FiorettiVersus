package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public class TitleScreen implements Screen{
    final ProjectF game;
   
    private TextureRegion[] title = new TextureRegion[5];
    private Texture titles;
    private Texture cursor;
	private int page = 0;
	private int selectedRow = Constant.BATTLE_SELECT;
	
	public TitleScreen(final ProjectF game){
		this.game = game;
		titles = game.getManager().get(AssetConstant.TITLE_BG, Texture.class);
		cursor = game.getManager().get(AssetConstant.TITLE_CURSOR, Texture.class);
		title[0] = new TextureRegion(titles,0,0,640,360); //BG
		title[1] = new TextureRegion(titles,0,360,247,61); //PRESSENTER
		title[2] = new TextureRegion(titles,0,421,154,68); //BATTLE
		
		title[3] = new TextureRegion(titles,154,421,154,68); //TRAINING
		title[4] = new TextureRegion(titles,308,421,154,68); //SETTINGS
	}

//===== RENDER
	@Override
	public void render(float delta) {
		game.batch.begin();
		drawUi();
		game.batch.end();
		keyInputs();

	}
//===== RENDER
	
	private void keyInputs(){
		int[] info = game.getInputs().getTitleInputs(page, selectedRow);
		page = info[0];
		selectedRow = info[1];
	}
	
	private void drawUi(){		
		game.batch.draw(title[0], 0, 0);
		game.font.draw(game.batch, "version "+Constant.GAME_VERSION, 10, 20);
		switch(page){
		case 0:
			game.batch.draw(title[1],196,45);
			break;
		case 1:
			game.batch.draw(cursor, 225, Constant.OPTIONS_Y+selectedRow*Constant.TITLE_CURSOR_MOVE);
			drawSelection();
//			game.batch.draw(title[2], 243, 40);			
			break;
		}
	}
	
	private void drawSelection(){
		game.font.setColor(Color.WHITE);
		for(int n=0;n<Constant.TITLE_OPTION.length;n++){
			game.font.draw(game.batch, Constant.TITLE_OPTION[n], 270, Constant.OPTIONS_Y+35+n*Constant.TITLE_CURSOR_MOVE);
		}
	}

	@Override
	public void resize(int width, int height){}
	@Override
	public void show(){
		Sounds.getOp().setVolume(NewSettings.getMastervol());
		Sounds.getOp().play();
		Sounds.getOp().setLooping(true);
	}
	@Override
	public void hide(){}
	@Override
	public void pause(){}
	@Override
	public void resume(){}
	@Override
	public void dispose(){}
}