package com.mygdx.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public final class CharacterSelectTournament extends CharacterSelectSuper{

	private Texture background;
	private TextureRegion cursor = new TextureRegion(cursors,70,0,70,52);
	
	public CharacterSelectTournament(ProjectF game) {
		super(game);
		background = game.getManager().get(AssetConstant.CHAR_SEL_TOURNAMENT, Texture.class);
		game.getSettings().setMode(1); // OLD
		game.getBattleSettings().setTournamentMode();
		game.getSettings().resetTournamentCursor();
		NewSettings.bracket8.clear();
	}
	
	@Override 
	public void render(float delta){
		game.batch.begin();
			game.batch.draw(background, 0, 0);
			game.font.draw(game.batch,"- TOURNAMENT",320,gameModeY);			super.render(delta);
			game.batch.draw(cursor,game.getSettings().getBpos()[0],game.getSettings().getBpos()[1]);
			drawPool();
		game.batch.end();
		try{
			keyInputs();
		}catch(RuntimeException e){
			e.printStackTrace();
		}	
	}

	private void drawPool(){
		for (int i=0;i<NewSettings.bracket8.size();i++){
			game.batch.draw(icon[NewSettings.bracket8.get(i)], 5+(i%2)*Constant.CURSOR_MOVE_X, 64+(int)(Constant.CURSOR_MOVE_Y*(i-i%2)/2));
		}
	}
	
	private void keyInputs(){
		switch(game.getInputs().getCharSelTournamentInput()){
		case Constant.DISPOSE_ACTION:
			bgm.stop();
			dispose();
			break;
		}
	}

}
