package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.ProjectF;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public final class CharacterSelectVersus extends CharacterSelectSuper implements Screen {

	protected TextureRegion[] charSelectBg = new TextureRegion[2];
	protected Texture charSelect;
	protected TextureRegion[] cursor = new TextureRegion[4];

	public CharacterSelectVersus(ProjectF game) {
		super(game);
		initBackground();
		initCursors();
		game.getSettings().setMode(0);
		game.getBattleSettings().setVersusMode();
		game.getSettings().resetVersusCursor();
	}
	
	private void initBackground() {
		charSelect = game.getManager().get(AssetConstant.CHAR_SEL_BG, Texture.class);
		charSelectBg[0] = new TextureRegion(charSelect,0,0,640,360);
		charSelectBg[1] = new TextureRegion(charSelect,0,360,640,360);
	}

	@Override
	public void render(float delta) {
		game.batch.begin();
			draw();
			super.render(delta);
			drawCursors();
		game.batch.end();
		try{
		keyInputs();
		}catch(RuntimeException e){e.printStackTrace();}
	}
	
	private void initCursors(){
		cursor[0] = new TextureRegion(cursors,0,0,70,52); //red
		cursor[3] = new TextureRegion(cursors,70,0,70,52);//blue
		cursor[2] = new TextureRegion(cursors,0,52,70,52);//half
		cursor[1] = new TextureRegion(cursors,70,52,70,52);//grey
	}
	
	private void drawCursors(){
		int cursorNum = 0;
		if(game.getBattleSettings().isVsCpu())
			cursorNum = 1;
		game.batch.draw(cursor[cursorNum],game.getSettings().getRpos()[0],game.getSettings().getRpos()[1]);

		TextureRegion bcursor = cursor[3];
		if (game.getSettings().getMode()!=1 && game.getSettings().getRpos()[0]==game.getSettings().getBpos()[0] && game.getSettings().getRpos()[1]==game.getSettings().getBpos()[1])
			bcursor = cursor[2];	
		game.batch.draw(bcursor,game.getSettings().getBpos()[0],game.getSettings().getBpos()[1]);
		
	}
	
	private void drawRedPortrait(){
		game.batch.draw(portrait[getSlot(game.getSettings().getRpos())], 0, 144);
		game.font.setColor(Color.BLACK);
		game.font.draw(game.batch, Constant.NAME[getSlot(game.getSettings().getRpos())], 35, 137);
	}
	
	private void keyInputs(){
		switch(game.getInputs().getCharSelVersusInput()){
		case Constant.NO_ACTION:
			break;
		case Constant.DISPOSE_ACTION:
			bgm.stop();
			dispose();			
			break;
		}
	}
	
	private void draw(){
		int bgNum = 0;
		if(game.getBattleSettings().isVsCpu())
			bgNum = 1;
		game.batch.draw(charSelectBg[bgNum],0,0);
		game.font.setColor(Color.WHITE);
		game.font.draw(game.batch,"- VERSUS",320,gameModeY);
		drawRedPortrait();

	}

}
