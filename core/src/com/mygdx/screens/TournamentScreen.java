package com.mygdx.screens;

import java.util.Collections;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public class TournamentScreen implements Screen{
	private final ProjectF game;
	
	private Texture background;
	private TextureRegion[] icon = new TextureRegion[16];
	private Texture icons;
	private Texture cross;

	
	public TournamentScreen(ProjectF game){
		this.game = game;
		background = game.getManager().get(AssetConstant.BRACKET_BG, Texture.class);
		cross = game.getManager().get(AssetConstant.BRACKET_CROSS, Texture.class);
		initIcons();
		if(NewSettings.playoff==0 && NewSettings.battle==0){
			Collections.shuffle(NewSettings.bracket8);
		}
//		System.out.println("bracket8: "+NewSettings.bracket8);
//		System.out.println("bracket4: "+NewSettings.bracket4);
//		System.out.println("bracket2: "+NewSettings.bracket2);

	}
	
	@Override
	public void render(float delta) {

		game.batch.begin();
		game.batch.draw(background, 0, 0);
		initIcons();
		drawBrackets();
		game.batch.end();
		keyInputs();
	}
	
	private void keyInputs() {
		if(game.getInputs().getTournamentInput()){
			this.dispose();
		}
	}

	private void initIcons(){
		icons = game.getManager().get(AssetConstant.CHAR_SEL_ICONS, Texture.class);
		int n=0;
		while (n<Constant.NUMBER_OF_CHAR+1){
			for (int i=0;i<Constant.CHAR_GRID_ROW;i++){
				for (int j=0;j<Constant.CHAR_GRID_COL;j++){
					icon[n] = new TextureRegion(icons,70*j,(52*i),70,52);
					n++;
				}				
			}			
		}
	}
	
	private void drawBracket8(){
		int offset = 0;
		for (int i=0;i<NewSettings.bracket8.size();i++){
			if(NewSettings.bracket8.get(i)>-1){
				if(i>3)
					offset=1;
				if(i%2==0){
					game.batch.draw(icon[NewSettings.bracket8.get(i)],offset+1+165*i/2,19);
					if(!NewSettings.bracket4.contains(NewSettings.bracket8.get(i)) && NewSettings.bracket8.get(i+1)==-1)
						game.batch.draw(cross,offset+1+165*i/2,19);
				}else{
					game.batch.draw(icon[NewSettings.bracket8.get(i)],offset+73+165*(i-1)/2,19);
					if(!NewSettings.bracket4.contains(NewSettings.bracket8.get(i)) && NewSettings.bracket8.get(i-1)==-1)
						game.batch.draw(cross,offset+73+165*(i-1)/2,19);
				}
			}
		}
	}
	
	private void drawBracket4(){
		if(!NewSettings.bracket4.isEmpty()){
			int offset = 0;
			for (int i=0;i<NewSettings.bracket4.size();i++){
				if(NewSettings.bracket4.get(i)>-1){
					if(i>1)
						offset=1;
					if(i%2==0){
						game.batch.draw(icon[NewSettings.bracket4.get(i)],offset+38+329*i/2,105);
						if(NewSettings.bracket4.size()==4 && !NewSettings.bracket2.contains(NewSettings.bracket4.get(i)) && NewSettings.bracket4.get(i+1)==-1)
							game.batch.draw(cross,offset+38+329*i/2,105);
					}else{
						game.batch.draw(icon[NewSettings.bracket4.get(i)],offset+203+329*(i-1)/2,105);
						if(NewSettings.bracket4.size()==4 && !NewSettings.bracket2.contains(NewSettings.bracket4.get(i)) && NewSettings.bracket4.get(i-1)==-1)
							game.batch.draw(cross,offset+203+329*(i-1)/2,105);
					}
				}
			}				
		}

	}
	
	private void drawBracket2(){
		if(!NewSettings.bracket2.isEmpty()){
			for (int i=0;i<NewSettings.bracket2.size();i++){
				if(NewSettings.bracket2.get(i)>-1){
					game.batch.draw(icon[NewSettings.bracket2.get(i)],122+326*i,179);
					if(NewSettings.winner>-1){
						game.batch.draw(cross,122+326*i,179);
					}
				}

			}
		}
	}

	private void drawBrackets(){
		drawBracket8();
		drawBracket4();
		drawBracket2();

		if(NewSettings.winner>-1){
			game.batch.draw(icon[NewSettings.winner], 285, 295);
			drawWinner();
		}
	}
	
	private Texture firstPlace = new Texture(Gdx.files.internal("graphics/tournamentWinner.png"));
	private void drawWinner(){
		int winnerSlot = NewSettings.winner;
		int j = winnerSlot%4;
		int i = winnerSlot/4;
		Texture portraits = game.getManager().get(AssetConstant.CHAR_SEL_PORTRAITS, Texture.class);
		TextureRegion portrait = new TextureRegion(portraits,256*j,768-(256*i),145,145);

		game.font.getData().setScale(1.25f);
		game.batch.draw(portrait, 250, 105);
		game.batch.draw(firstPlace, 0, 0);
		game.font.setColor(Color.WHITE);
		game.font.draw(game.batch, Constant.NAME[winnerSlot], 280, 80);
		game.font.getData().setScale(1);
	}
	
	@Override
	public void show() {
		if(NewSettings.winner<0)
			Sounds.getBracket().play(NewSettings.getMastervol());
		else
			Sounds.champion.play(NewSettings.getMastervol());
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}
