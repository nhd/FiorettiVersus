package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.model.AssetConstant;
import com.mygdx.model.Constant;

public abstract class CharacterSelectSuper implements Screen{
	final ProjectF game;
	protected Music bgm;
	protected TextureRegion[] portrait = new TextureRegion[16];
	protected Texture portraits;
	protected TextureRegion[] icon = new TextureRegion[16];
	protected Texture icons;
	protected Texture cursors;
	
	protected int gameModeY = 321;
	
	public CharacterSelectSuper(ProjectF game) {
		this.game = game;
		getAssets();
		initTextures();
		initIcons();
	}
	
	@Override
	public void render(float delta){
		drawIcons();
		drawPortraits();
		drawGameMode();
	}
	
	private void getAssets(){
		bgm = game.getManager().get(AssetConstant.CHAR_SEL_BG_MUSIC, Music.class);
		portraits = game.getManager().get(AssetConstant.CHAR_SEL_PORTRAITS, Texture.class);
		icons = game.getManager().get(AssetConstant.CHAR_SEL_ICONS, Texture.class);
		cursors = game.getManager().get(AssetConstant.CHAR_SEL_CURSORS, Texture.class);
	}
	
	private void initTextures(){
		int n=0;
		while (n<Constant.NUMBER_OF_CHAR+1){
			for (int i=0;i<Constant.CHAR_GRID_ROW;i++){
				for (int j=0;j<Constant.CHAR_GRID_COL;j++){
					portrait[n] = new TextureRegion(portraits,256*j,768-(256*i),145,145);
					n++;
				}				
			}			
		}
	}

	private void initIcons(){
		int n=0;
		while (n<Constant.NUMBER_OF_CHAR+1){
			for (int i=0;i<Constant.CHAR_GRID_ROW;i++){
				for (int j=0;j<Constant.CHAR_GRID_COL;j++){
					icon[n] = new TextureRegion(icons,70*j,(52*i),70,52);
					n++;
				}				
			}			
		}
	}
	
	private void drawIcons(){
		for (int i=0;i<Constant.CHAR_GRID_ROW;i++){
			for (int j=0;j<Constant.CHAR_GRID_COL;j++){
//				if(game.getSettings().getMode()!=1 || i*4+j!=3)
					game.batch.draw(icon[i*4+j], Constant.CURSOR_MIN_X+j*Constant.CURSOR_MOVE_X, Constant.CURSOR_MIN_Y+Constant.CURSOR_MOVE_Y*i);
			}
		}
	}
	
	private void drawPortraits(){
		game.batch.draw(portrait[getSlot(game.getSettings().getBpos())], 472, 144);
		game.font.setColor(Color.BLACK);
		game.font.draw(game.batch, Constant.NAME[getSlot(game.getSettings().getBpos())], 505, 137);
	}

	protected int getSlot(int[] pos){
		int xcord = (pos[0]-Constant.CURSOR_MIN_X)/Constant.CURSOR_MOVE_X;
		int ycord = ((pos[1]-Constant.CURSOR_MIN_Y)/Constant.CURSOR_MOVE_Y);
		return xcord+ycord*Constant.CHAR_GRID_ROW;
	}
	
	private void drawGameMode(){
		game.font.setColor(Color.WHITE);
		int gameModeX = 235;
		if (game.getSettings().getBestof()!=0)
			gameModeX = 238;
		game.font.draw(game.batch,Constant.BATTLE_RULE[game.getSettings().getRule()], gameModeX, gameModeY);

	}
	
	@Override
	public void resize(int width, int height){}
	@Override
	public void show(){
		bgm.setVolume(NewSettings.getMastervol());
		bgm.setLooping(true);
		bgm.play();
	}
	@Override
	public void hide(){}
	@Override
	public void pause(){}
	@Override
	public void resume(){}
	@Override
	public void dispose(){
		bgm.dispose();
	}
}
