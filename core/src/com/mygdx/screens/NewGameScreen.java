package com.mygdx.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.*;
import com.mygdx.model.Constant;

//removed or disabled : music manager, special, inputs

public class NewGameScreen implements Screen {
	final ProjectF game;

	private OrthographicCamera cam = new OrthographicCamera(640, 360);

	private Fighter red;
	private Fighter blue;

	private String phase = "ready";
	private long startTime;

	private Music bgm;

	private BattleHud display;
	private boolean leave = false;

	private ArrayList<Integer> bracket;
	
	int tournamentBlueSlot;
	int tournamentRedSlot;
	
	// TEST
	// private boolean arcade = false;
	// private int[] opponents = {0,1,2,4,5};
	// private int round = 0;
	// private String[] names =
	// {"SARAH","TIMOTHEE","THOMAS","MAREVA","AURIELLE","BAPTISTE","BRUNO","CAMILLE"};

	public NewGameScreen(final ProjectF game) {
		this.game = game;
		switch(NewSettings.playoff){
		case 0:
			this.bracket = NewSettings.bracket8;
			break;
		case 1:
			this.bracket = NewSettings.bracket4;
			break;
		case 2:
			this.bracket = NewSettings.bracket2;
			break;
		}		
		
		if(game.getSettings().getMode()==1){
			tournamentBlueSlot = bracket.get(NewSettings.battle*2+1);
			blue = game.getSettings().getFighter(tournamentBlueSlot);
		}else{
//			blue = game.getSettings().getFighter(blueSlot);
			blue = game.getBattleSettings().getBluePlayer();
		}
		
		
		opponentSetup();
		blue.x = 640 - blue.getWidth() - 50;

		game.getInputs().setBattleInputs(red, blue);
		display = new BattleHud(game, red, blue);

		cam.position.set(320, 180, 0);
		BattleHud.setShake(false);
		startTime = TimeUtils.millis();

	}
	
	private void bgmSetup(){
		if(game.getBattleSettings().getMode()==Constant.TOURNAMENT_MODE && NewSettings.playoff==2){
			bgm = Sounds.finalRound;
		}else{
			bgm = Sounds.bgmSelector();

		}
		bgm.setVolume(NewSettings.getMastervol());
		bgm.play();
		bgm.setLooping(true);
	}

	private void opponentSetup() {
		if(game.getSettings().getMode()!=1){
//			red = game.getSettings().getFighter(slot);
			red = game.getBattleSettings().getRedPlayer();
		}else{
			tournamentRedSlot = bracket.get(NewSettings.battle*2);
			red = game.getSettings().getFighter(tournamentRedSlot);
		}
	}

	private void resetPosition() {
		// int dice = MathUtils.random(0,7);
		// this.blue = new Player(names[dice],opponents[dice]);
		// if (game.getSettings().random) game.getSettings().stage =
		// MathUtils.random(1,25);
		Sounds.getReadysound().play(NewSettings.getMastervol() * 2);
		red.reset(50);
		blue.reset(590 - (int) blue.getData()[0]);
		startTime = TimeUtils.millis();
		resetRedSpecial();
		try {
			blue.getSpecialAttack().stop();
		} catch (Exception e) {
		}
	}

	private void resetRedSpecial() {
		try {
			red.getSpecialAttack().stop();
		} catch (Exception e) {
		}
	}

	// ========== RENDER
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		cam.update();
		game.batch.setProjectionMatrix(cam.combined);

		display.draw(red, blue);

		display.shake(cam, game.getSettings().getShakeIntensity());
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

		display.drawShapes(red, blue);

		if (phase.equals("play") && (blue.getHealth() <= 0 || red.getHealth() <= 0))
			phase = "end";
		phases();

	}

	// ========== RENDER_END
	private void phases() {
		switch (phase) {
		case "ready":
			phase = display.readyGo(startTime);
			break;
		case "play":
			leave = game.getInputs().getBattleInputs();
			exit();
			checkingBlue();
			checkingRed();
			break;
		case "end":
			phase = display.result(red, blue);
			startTime = TimeUtils.millis();
			break;
		case "next":
			nextBattle();
			break;
		}
	}

	private void exit() {
		if (leave) {
			cam.position.set(320, 180, 0);
			dispose();
			display.dispose();
			game.getSettings().setStageReady(false);
			game.getInputs().cancelAction();
		}
	}

	private void checkingBlue() {
		blue.hitCheck(red);
		blue.recoveryUpdate();
		blue.getSpecialAttack().updateState();
	}

	private void checkingRed() {
		red.hitCheck(blue);
		red.recoveryUpdate();
		red.getSpecialAttack().updateState();
	}

	private boolean endBattleCheck() {
		boolean end = false;
			if (game.getSettings().getBestof() == 0
					&& (red.getVictory() == game.getSettings().getFirstto()
							|| blue.getVictory() == game.getSettings().getFirstto())
					|| (red.getVictory() > game.getSettings().getBestof() / 2
							|| blue.getVictory() > game.getSettings().getBestof() / 2)
							&& game.getSettings().getFirstto() == 0) {
				end = true;
			}
		return end;
	}

	private void nextBattle() {
		boolean end = false;
		display.endBattleMessage();
		if (TimeUtils.millis() - startTime > 1250) {
			end = endBattleCheck();
			if (end) {
				bgm.stop();
				dispose();
				if(game.getSettings().getMode()!=1){
					game.setScreen(new CharacterSelectVersus(game));
					game.getSettings().setStageReady(false);
				}else{
					int winner;
										
					switch(NewSettings.playoff){
					case 0:
						if(blue.getVictory()>red.getVictory()){
							winner = tournamentBlueSlot;			
							NewSettings.bracket8.set(NewSettings.battle*2+1, -1);
						}else{
							winner = tournamentRedSlot;
							NewSettings.bracket8.set(NewSettings.battle*2, -1);
						}		
						NewSettings.bracket4.add(winner);
						break;
					case 1:
						if(blue.getVictory()>red.getVictory()){
							winner = tournamentBlueSlot;			
							NewSettings.bracket4.set(NewSettings.battle*2+1, -1);
						}else{
							winner = tournamentRedSlot;
							NewSettings.bracket4.set(NewSettings.battle*2, -1);
						}		
						NewSettings.bracket2.add(winner);
						break;
					case 2:
						if(blue.getVictory()>red.getVictory()){
							winner = tournamentBlueSlot;			
							NewSettings.bracket2.set(1, -1);
						}else{
							winner = tournamentRedSlot;	
							NewSettings.bracket2.set(0, -1);
						}		
						NewSettings.winner = winner;
						break;
					}
					NewSettings.battle++;
					switch(NewSettings.playoff){
					case 0:
						if(NewSettings.battle==4){
							NewSettings.battle=0;
							NewSettings.playoff=1;
						}							
						break;
					case 1:
						if(NewSettings.battle==2){
							NewSettings.battle=0;
							NewSettings.playoff=2;
						}							
						break;
					case 2:
						NewSettings.battle=0;
						NewSettings.playoff=3;					
						break;
					}
					game.setScreen(new TournamentScreen(game));
				}
			} else {
				phase = "ready";
				resetPosition();
			}
		}
	}

	// ===== OVERRIDE =====//
	@Override
	public void resize(int width, int height) {
		cam.update();
	}

	@Override
	public void show() {
		Sounds.getReadysound().play(NewSettings.getMastervol() * 2);
		bgmSetup();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		bgm.dispose();
	}
}
