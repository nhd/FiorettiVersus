package com.mygdx.controller;

import com.mygdx.game.ProjectF;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;
import com.mygdx.screens.CharacterSelectTournament;
import com.mygdx.screens.CharacterSelectVersus;

public class MasterInput {
	private final ProjectF game;
	private StageInput stageInput;
	private TitleInput titleInput;
	private SettingsInput settingsInput;
	private BattleInput battleInput;
	private TournamentInput tournamentInput;
	
	private CharacterSelectVersusInput charSelVersusInput;
	private CharacterSelectTournamentInput charSelTournamentInput;
	
	public MasterInput(ProjectF game){
		this.game = game;
		this.stageInput = new StageInput(game);
		this.titleInput = new TitleInput(game);
		this.settingsInput = new SettingsInput(game);
		this.battleInput = new BattleInput(game);
		this.tournamentInput = new TournamentInput(game);
		this.charSelVersusInput = new CharacterSelectVersusInput(game);
		this.charSelTournamentInput = new CharacterSelectTournamentInput(game);
	}
	
	public int[] getTitleInputs(int num, int row){
		return titleInput.getitleInput(num, row);
	}
	
	public boolean getStageInput(int numberOfStages){
		return stageInput.getStageInput(numberOfStages);
	}
	
	public void setBattleInputs(Fighter red, Fighter blue){
		battleInput.setBattleInputs(red, blue);
	}
	
	public boolean getBattleInputs(){
		return battleInput.getBattleInput();
	}
	
	public int getSettingsInputs(int y){
		return settingsInput.getSettingsInput(y);
	}
	
	public int getCharSelVersusInput(){
		return charSelVersusInput.getCharSelVersusInput();
	}
	
	public int getCharSelTournamentInput(){
		return charSelTournamentInput.getCharSelTournamentInput();
	}
	
	public boolean getTournamentInput(){
		return tournamentInput.getTournamentInputs();
	}
	
	public void cancelAction(){
		switch(game.getBattleSettings().getMode()){
		case Constant.VERSUS_MODE:
			game.setScreen(new CharacterSelectVersus(game));
			break;
		case Constant.TOURNAMENT_MODE:
			game.setScreen(new CharacterSelectTournament(game));
			break;
		}	
	}
	
}
