package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.Constant;
import com.mygdx.screens.NewGameScreen;
import com.mygdx.screens.TournamentScreen;

public class StageInput {
	private final ProjectF game;
	
	public StageInput(ProjectF game){
		this.game = game;
	}
	
	public boolean getStageInput(int numberOfStages){
		boolean dispose = false;
		if (Gdx.input.isKeyJustPressed(Keys.RIGHT)){
			Sounds.getBeep().play(NewSettings.getMastervol());
			game.getSettings().setStage(game.getSettings().getStage()+1);
			if (game.getSettings().getStage()==numberOfStages) game.getSettings().setStage(1);
		}
		else if (Gdx.input.isKeyJustPressed(Keys.LEFT)){
			Sounds.getBeep().play(NewSettings.getMastervol());
			game.getSettings().setStage(game.getSettings().getStage()-1);
			if (game.getSettings().getStage()==0) game.getSettings().setStage(numberOfStages-1);
		}
//		else if (Gdx.input.isKeyJustPressed(Keys.DOWN)){
//			game.getSettings().nextTrack();
//		}
//		else if (Gdx.input.isKeyJustPressed(Keys.UP)){
//			game.getSettings().previousTrack();
//		}
		else if ( Gdx.input.isKeyJustPressed(Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Keys.BACKSPACE) ){
			dispose = true;
			Sounds.getCancel().play(NewSettings.getMastervol());
			isRandomStage(numberOfStages);
			game.getInputs().cancelAction();
		}
		else if (Gdx.input.isKeyJustPressed(Keys.ENTER)){
			dispose = true;
			Sounds.getSelect().play(NewSettings.getMastervol());
			isRandomStage(numberOfStages);
			if (game.getSettings().isRandomStage()){
				game.getSettings().setStage( MathUtils.random(1,numberOfStages-2));
			}
			game.getSettings().setStageReady(false);
			if(game.getSettings().getMode()==Constant.VERSUS_MODE){
				game.setScreen(new NewGameScreen(game));
			}else
				game.setScreen(new TournamentScreen(game));

		}
		return dispose;
	}
	
	private void isRandomStage(int numberOfStages){
		if (game.getSettings().getStage()==numberOfStages-1){
			game.getSettings().setRandomStage(true);
		}else{
			game.getSettings().setRandomStage(false);
		}
	}
}
