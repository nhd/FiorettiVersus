package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.game.ProjectF;
import com.mygdx.game.fighters.Fighter;

public class BattleInput {
	private final ProjectF game;
	private Fighter red;
	private Fighter blue;
	private boolean end = false;

	public BattleInput(ProjectF game) {
		this.game = game;
	}

	public void setBattleInputs(Fighter red, Fighter blue) {
		this.end = false;
		this.red = red;
		this.blue = blue;
	}

	public boolean getBattleInput() {
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)){
			end = true;
		}
		blueInputs();
		redInputs();
		return end;
	}

	private void blueInputs() {
//		blue.cpuAction(red);
		if (Gdx.input.isKeyPressed(Keys.DOWN) && Gdx.input.isKeyPressed(Keys.RIGHT))
			blue.dashRight();
		else if (Gdx.input.isKeyPressed(Keys.DOWN) && Gdx.input.isKeyPressed(Keys.LEFT))
			blue.dashLeft();

		if (Gdx.input.isKeyPressed(Keys.LEFT))
			blue.moveLeft();
		else if (Gdx.input.isKeyPressed(Keys.RIGHT))
			blue.moveRight();

		if (Gdx.input.isKeyJustPressed(Keys.UP)) {
			blue.attack(red);
		} else if (Gdx.input.isKeyPressed(Keys.DOWN))
			blue.evade();
	}

	private void redInputs() {
//		red.cpuAction(blue);
		if (!game.getBattleSettings().isVsCpu()) {
			if (Gdx.input.isKeyPressed(Keys.S) && Gdx.input.isKeyPressed(Keys.D))
				red.dashRight();
			else if (Gdx.input.isKeyPressed(Keys.S) && Gdx.input.isKeyPressed(Keys.Q))
				red.dashLeft();
			if (Gdx.input.isKeyPressed(Keys.Q))
				red.moveLeft();
			else if (Gdx.input.isKeyPressed(Keys.D))
				red.moveRight();
			if (Gdx.input.isKeyJustPressed(Keys.Z)) {
				red.attack(blue);
			} else if (Gdx.input.isKeyPressed(Keys.S))
				red.evade();
		} else {
			red.cpuAction(blue);
		}
	}
}
