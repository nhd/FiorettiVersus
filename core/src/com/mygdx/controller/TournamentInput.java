package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.screens.CharacterSelectTournament;
import com.mygdx.screens.NewGameScreen;

public class TournamentInput {
	private final ProjectF game;
	
	public TournamentInput(ProjectF game){
		this.game = game;
	}
	
	public boolean getTournamentInputs(){
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Keys.BACKSPACE)){
			try{
				game.setScreen(new CharacterSelectTournament(game));
				game.getSettings().setStageReady(false);
				NewSettings.bracket8.clear();
				NewSettings.bracket4.clear();
				NewSettings.bracket2.clear();
				NewSettings.winner = -1;
				NewSettings.playoff = 0;
				NewSettings.battle = 0;
				Sounds.getBracket().stop();
				return true;
			}catch(RuntimeException e){
				e.getStackTrace();
			}
		}else if(Gdx.input.isKeyJustPressed(Keys.ENTER)){
			Sounds.getBracket().stop();
			if(NewSettings.playoff!=3){
				game.setScreen(new NewGameScreen(game));
			}else{
				game.setScreen(new CharacterSelectTournament(game));
				game.getSettings().setStageReady(false);
				NewSettings.bracket8.clear();
				NewSettings.bracket4.clear();
				NewSettings.bracket2.clear();
				NewSettings.winner = -1;
				NewSettings.playoff = 0;
				NewSettings.battle = 0;
				return true;
			}
		}
		
		return false;
	}
}
