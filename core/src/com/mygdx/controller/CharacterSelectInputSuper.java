package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.Constant;
import com.mygdx.screens.TitleScreen;

public abstract class CharacterSelectInputSuper {
	protected final ProjectF game;
	
	public CharacterSelectInputSuper(ProjectF game){
		this.game = game;
	}
	
	protected void moveBlueCursor(){
		if (Gdx.input.isKeyJustPressed(Keys.RIGHT)) 
			game.getSettings().getBpos()[0] += Constant.CURSOR_MOVE_X;
		if (Gdx.input.isKeyJustPressed(Keys.LEFT)) 
			game.getSettings().getBpos()[0] -= Constant.CURSOR_MOVE_X;
		if (Gdx.input.isKeyJustPressed(Keys.UP)) 
			game.getSettings().getBpos()[1] += Constant.CURSOR_MOVE_Y;
		if (Gdx.input.isKeyJustPressed(Keys.DOWN)) 
			game.getSettings().getBpos()[1] -= Constant.CURSOR_MOVE_Y;
		game.getSettings().setBpos(cursorLimit(game.getSettings().getBpos()));
	}
	
	protected boolean cancelAction(){
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Keys.BACKSPACE)){ 
			Sounds.getCancel().play(NewSettings.getMastervol());
			game.setScreen(new TitleScreen(game));
			return true;
		}
		return false;
	}
	
	protected void changeBattleRule(){
		if (Gdx.input.isKeyJustPressed(Keys.F2)){
			game.getSettings().nextRule();
		}
	}
	
	protected int getSlotFinal(int[] pos){
		int xcord = (pos[0]-Constant.CURSOR_MIN_X)/Constant.CURSOR_MOVE_X;
		int ycord = ((pos[1]-Constant.CURSOR_MIN_Y)/Constant.CURSOR_MOVE_Y);

		int slot = xcord+ycord*Constant.CHAR_GRID_ROW;
		if (slot==Constant.RAND_CHAR_SLOT && game.getSettings().getMode()!=1){
			while(slot==Constant.RAND_CHAR_SLOT){
				slot = MathUtils.random(1,Constant.NUMBER_OF_CHAR);
			}
		}
		return slot;
	}
	
	private int[] keybeepRed = {45,32,47,54};
	private int[] keybeepBlue = {19,20,21,22};
	protected void charSelKeyBeeper(){
		for (int k=0;k<keybeepRed.length;k++){
			if (Gdx.input.isKeyJustPressed(keybeepRed[k]) && game.getSettings().getMode()==Constant.VERSUS_MODE || Gdx.input.isKeyJustPressed(keybeepBlue[k]))
				Sounds.getBeep().play(NewSettings.getMastervol());
		}		
	}	
	
	protected int[] cursorLimit(int[] pos){
		if (pos[0]<Constant.CURSOR_MIN_X) pos[0] = Constant.CURSOR_MAX_X;
		if (pos[0]>Constant.CURSOR_MAX_X) pos[0] = Constant.CURSOR_MIN_X;
		if (pos[1]>Constant.CURSOR_MAX_Y) pos[1] = Constant.CURSOR_MIN_Y;
		if (pos[1]<Constant.CURSOR_MIN_Y) pos[1] = Constant.CURSOR_MAX_Y;
		return pos;
	}
}
