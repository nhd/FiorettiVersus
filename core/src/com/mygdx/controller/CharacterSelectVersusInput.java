package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.Constant;
import com.mygdx.screens.StagesScreen;

public class CharacterSelectVersusInput extends CharacterSelectInputSuper{
	
	public CharacterSelectVersusInput(ProjectF game){
		super(game);
	}
	
	public int getCharSelVersusInput(){
		int command = Constant.NO_ACTION;
		changeBattleRule();
		charSelKeyBeeper();
		moveBlueCursor();
		moveRedCursor();
		
		if (Gdx.input.isKeyJustPressed(Keys.F1)){
			game.getBattleSettings().toggeVsCpu();
		}else if(Gdx.input.isKeyJustPressed(Keys.ENTER) && checkPlayable()){
			command = Constant.DISPOSE_ACTION;
			game.getBattleSettings().saveFightersSlot(getBlueSlot(), getRedSlot());
			game.setScreen(new StagesScreen(game));
			game.getBattleSettings().setupFighters();
			Sounds.getSelect().play(NewSettings.getMastervol());
		}
		
		if(cancelAction())
			command = Constant.DISPOSE_ACTION;

		return command;
	}
	
	private boolean checkPlayable(){
		if(NewSettings.isPlayable(Constant.NAME[getSlotFinal(game.getSettings().getBpos())]) && NewSettings.isPlayable(Constant.NAME[getSlotFinal(game.getSettings().getRpos())]))
			return true;
		return false;
	}
	
	private int getBlueSlot(){
		return getSlotFinal(game.getSettings().getBpos());
	}
	
	private int getRedSlot(){
		return getSlotFinal(game.getSettings().getRpos());
	}
	
	private void moveRedCursor(){
		if (Gdx.input.isKeyJustPressed(Keys.Q)) 
			game.getSettings().getRpos()[0] -= Constant.CURSOR_MOVE_X;
		if (Gdx.input.isKeyJustPressed(Keys.D)) 
			game.getSettings().getRpos()[0] += Constant.CURSOR_MOVE_X;
		if (Gdx.input.isKeyJustPressed(Keys.S)) 
			game.getSettings().getRpos()[1] -= Constant.CURSOR_MOVE_Y;
		if (Gdx.input.isKeyJustPressed(Keys.Z)) 
			game.getSettings().getRpos()[1] += Constant.CURSOR_MOVE_Y;
		game.getSettings().setRpos(cursorLimit(game.getSettings().getRpos()));

	}
}
