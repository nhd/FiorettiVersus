package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.CpuModule;
import com.mygdx.screens.TitleScreen;

public class SettingsInput {
	final private ProjectF game;
	
	private final int FIRST_OPTION = 275;
	private final int STEP = 30;
	private final int NUM_OPTIONS = 2;
	
	public SettingsInput(ProjectF game){
		this.game = game;
	}
	
	public int getSettingsInput(int y){
		switch(y){
		case FIRST_OPTION:
			if (Gdx.input.isKeyPressed(Keys.LEFT)){
				game.getSettings().volumeDown();
				Sounds.getBgm().setVolume(NewSettings.getMastervol());
			}
			else if (Gdx.input.isKeyPressed(Keys.RIGHT)){
				game.getSettings().volumeUp();
				Sounds.getBgm().setVolume(NewSettings.getMastervol());
			}
			break;
		case FIRST_OPTION-STEP:
			if (Gdx.input.isKeyJustPressed(Keys.LEFT) || Gdx.input.isKeyJustPressed(Keys.RIGHT)){
				CpuModule.toggleAggressivity();
			}
			break;
		}

		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE) ||Gdx.input.isKeyJustPressed(Keys.BACKSPACE)){
			Sounds.getCancel().play(NewSettings.getMastervol());
			game.setScreen(new TitleScreen(game));
		}
		else if (Gdx.input.isKeyJustPressed(Keys.UP)){
			y+=STEP;
			if(y>FIRST_OPTION)
				y=FIRST_OPTION-NUM_OPTIONS*STEP;
		}
		else if (Gdx.input.isKeyJustPressed(Keys.DOWN)){
			y-=STEP;	
			if(y<FIRST_OPTION-NUM_OPTIONS*STEP)
				y=FIRST_OPTION;
		}
		
		return y;
	}
}
