package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.Constant;
import com.mygdx.screens.CharacterSelectTournament;
import com.mygdx.screens.CharacterSelectVersus;
import com.mygdx.screens.SettingsScreen;

public class TitleInput {
	private final ProjectF game;
	
	public TitleInput(ProjectF game) {
		this.game = game;
	}
	
	public int[] getitleInput(int page, int selectedRow){
		if (page==1){
			if (Gdx.input.isKeyJustPressed(Keys.DOWN)){
				Sounds.getBeep().play(NewSettings.getMastervol());
				selectedRow--;
				if (selectedRow==-1) selectedRow=Constant.TITLE_OPTION.length-1;
			}
			if (Gdx.input.isKeyJustPressed(Keys.UP)){
				Sounds.getBeep().play(NewSettings.getMastervol());
				selectedRow++;
				if (selectedRow==Constant.TITLE_OPTION.length) selectedRow=0;
			}
		}		
		if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			Sounds.getSelect().play(NewSettings.getMastervol());
			switch(page){
			case 0:
				page=1;
				break;
			case 1:
				switch(selectedRow){
				case Constant.BATTLE_SELECT:
					Sounds.getOp().stop();
					try{
						game.setScreen(new CharacterSelectVersus(game));
					}catch(RuntimeException e){
						e.printStackTrace();
					}
					
					break;
				case Constant.SETTINGS_SELECT:
					game.setScreen(new SettingsScreen(game));
					break;
				case Constant.TOURNAMENT_SELECT:
					Sounds.getOp().stop();
					game.setScreen(new CharacterSelectTournament(game));
					break;
				}
			}
		}
		int[] titleInfo = {page, selectedRow};
		return titleInfo;
	}
	
}
