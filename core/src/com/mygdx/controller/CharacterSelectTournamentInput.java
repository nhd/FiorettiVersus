package com.mygdx.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.ProjectF;
import com.mygdx.game.Sounds;
import com.mygdx.model.Constant;
import com.mygdx.screens.TournamentScreen;

public class CharacterSelectTournamentInput extends CharacterSelectInputSuper{
	
	public CharacterSelectTournamentInput(ProjectF game){
		super(game);
	}
	
	public int getCharSelTournamentInput(){
		int command = Constant.NO_ACTION;
		changeBattleRule();
		charSelKeyBeeper();
		moveBlueCursor();

		if (NewSettings.bracket8.size()!=8 && !NewSettings.bracket8.contains(getSlotFinal(game.getSettings().getBpos())) && Gdx.input.isKeyJustPressed(Keys.ENTER) && NewSettings.isPlayable(Constant.NAME[getSlotFinal(game.getSettings().getBpos())])){ 
			addToBracket();
		}else if(Gdx.input.isKeyJustPressed(Keys.R) && !NewSettings.bracket8.isEmpty()){
			NewSettings.bracket8.remove(NewSettings.bracket8.size()-1);
			Sounds.getCancel().play(NewSettings.getMastervol());	
		}else if(Gdx.input.isKeyJustPressed(Keys.ENTER) && NewSettings.bracket8.size()==8){
			command = Constant.DISPOSE_ACTION;
			game.setScreen(new TournamentScreen(game));
			Sounds.getSelect().play(NewSettings.getMastervol());
		}
		if(cancelAction())
			command = Constant.DISPOSE_ACTION;
		
		return command;
	}
	
	private void addToBracket(){
		if(getSlotFinal(game.getSettings().getBpos())==Constant.RAND_CHAR_SLOT){
			int rng;;
			do{
				rng = MathUtils.random(0,15);
			}while(rng==3 || NewSettings.bracket8.contains(rng));
			NewSettings.bracket8.add(rng);
		}else
			NewSettings.bracket8.add(getSlotFinal(game.getSettings().getBpos()));
		Sounds.getSelect().play(NewSettings.getMastervol());
	}
	
}

