package com.mygdx.game.fighters;

import com.mygdx.game.specials.DamiaanSpecial;

public class Damiaan extends Fighter{
	public Damiaan(){
		super("damiaan",8);
		spriteSetup();
		special = new DamiaanSpecial(this);
	}
}
