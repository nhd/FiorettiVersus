package com.mygdx.game.fighters;

import com.mygdx.game.specials.ThuhienSpecial;
import com.mygdx.model.Constant;

public class Thuhien extends Fighter{
	public Thuhien(){
		super("thu hien",3);
		spriteSetup();
		special = new ThuhienSpecial(this);
	}
}
