package com.mygdx.game.fighters;

import com.mygdx.game.specials.MytranSpecial;

public class Mytran extends Fighter{
	public Mytran(){
		super("my tran",5);
		spriteSetup();
		special = new MytranSpecial(this);
	}
}
