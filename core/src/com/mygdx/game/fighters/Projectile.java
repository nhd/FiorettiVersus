//package com.mygdx.game.Fighters;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.audio.Sound;
//import com.badlogic.gdx.graphics.Texture;
//import com.badlogic.gdx.graphics.g2d.TextureRegion;
//import com.badlogic.gdx.math.Rectangle;
//import com.mygdx.game.NewGameScreen;
//import com.mygdx.game.Settings;
//import com.mygdx.game.Sounds;
//
//public class Projectile extends Rectangle{
//	private String position = "right";
//	private static float speed = 15;
//	private float velocity = speed;
//	private NewPlayer target;
//	private NewPlayer user;
//	private TextureRegion sprite;
//	private Texture sheet;
////	private TextureRegion[] sprites = new TextureRegion[16];
//	
////	private Sound projhit = Gdx.audio.newSound(Gdx.files.internal("sounds/wallhit_fast.ogg"));
//	
//	public Projectile(NewPlayer user){
//		this.user = user;
//		setup();
//		width = sprite.getRegionWidth();
//		height = sprite.getRegionHeight();
//		y=150;
//	}
//
//	public void launch(String position, NewPlayer target){
//		this.position = position;
//		this.target = target;
//		if (position.equals("left")){
//			x = user.getX()+user.getData()[0];
//			velocity = speed;
//		}else{
//			x = user.getX()-user.getData()[0];
//			velocity = -speed;
//		}
//	}
//	
//	private void setup(){
//		sheet = new Texture(Gdx.files.internal("graphics/spell.png"));
//		sprite = new TextureRegion(sheet,0,0,80,80);
//	}
//
//	public void move(){
//		x += velocity;
//		hit();
//	}
//	
//	private void hit(){		
//		if (this.overlaps(target) && target.getRecoveryHit()==0 && !target.isInvincible()){
////			projhit.play(Settings);
//			Sounds.getHitsound().play(Settings.mastervol);
//			target.setHp(target.getHp()-4);
//			target.setRecoveryHit(50);
//			NewGameScreen.shake = true;
//			user.addCombo();
//			target.resetCombo();
//			System.out.println("kabooom");
//		}
//	}
//	
//	public boolean onScreen(){
//		boolean ok = true;
//		if (position.equals("left")){
//			if (x>640) ok = false;
//		}else {
//			if (x<-sprite.getRegionWidth()) ok = false;
//		}		
//		return ok;
//	}
//
//	public Rectangle getTarget() {
//		return target;
//	}
//
//	public TextureRegion getSprite() {
//		return sprite;
//	}
//
//	public String getPosition() {
//		return position;
//	}
//
//	public NewPlayer getUser() {
//		return user;
//	}
//	
//}
