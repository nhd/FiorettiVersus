package com.mygdx.game.fighters;

import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.TimotheeSpecial;

public class Timothee extends Fighter{
	public Timothee(){
		super("timothee",2);
		spriteSetup();
		special = new TimotheeSpecial(this);
		canAttackDuringSpecial = true;
	}

}
