package com.mygdx.game.fighters;

import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.CamilleSpecial;

public class Camille extends Fighter{
	
	public Camille(){
		super("camille",15);
		spriteSetup();
		special = new CamilleSpecial(this);
	}
	
	public void attack(Fighter target){
		if(canAct()){
			if (energy != 100) {
				Sounds.getMisssound().play(NewSettings.getMastervol());
				stance = "atk";
				actionCooldown = atkDuration;
			} else {
				Sounds.getProjsound().play(NewSettings.getMastervol());
				specialAttack(target);
			}	
		}
	}
	
	protected void specialAttack(Fighter target){
		setEnergy(0);
		special.cast(target);
	}
	
	public void hitCheck(Fighter target){
		if (stance.equals("atk") && getHurtbox().overlaps(target) && actionCooldown<atkDuration-5){ //&&special is moving
			if (!target.isInvincible() && !atkMissed && atk>0){					
				target.takeDamage(atk, combo);
				addCombo();
				staminaUp();
			}else if(target.stance.equals("eva") && atk>0){
				atkMissed = true;
			}
		}
	}
	
}
