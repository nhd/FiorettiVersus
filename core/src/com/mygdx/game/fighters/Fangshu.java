package com.mygdx.game.fighters;

import com.mygdx.game.specials.FangshuSpecial;

public class Fangshu extends Fighter{
	private int charge = 2;
	
	public Fangshu(){
		super("fang shu",9);
		spriteSetup();
		special = new FangshuSpecial(this);
	}
	
	protected void specialAttack(Fighter target){
		if(charge>0){
			charge--;
		}else{
			setEnergy(0);
			charge = 2;
		}
		special.cast(target);
		stance = "atk";
		atkMissed = true;
		actionCooldown = atkDuration;
	}
	
}
