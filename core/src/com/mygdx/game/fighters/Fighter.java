package com.mygdx.game.fighters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.*;
import com.mygdx.model.Constant;
import com.mygdx.screens.BattleHud;

public abstract class Fighter extends FighterBattleData{
	protected SpecialAttack special = null;
	protected CpuModule cpu = new CpuModule();

	public Fighter(String name, int slot){		
		super(name, slot);
	}
	
	public void cpuAction(Fighter target){
		cpu.play(this, target);
	}

	public void special(Fighter user, Fighter target){};
	
	public boolean canAct(){	
		return actionCooldown==0 && recoveryHit==0 || canAttackDuringSpecial && special.isActive() && actionCooldown==0 && recoveryHit==0;
	}
	
	protected boolean canMove(){		
		return !stance.equals("eva") && !stance.equals("spe");
	}
	
	public void attack(Fighter target){
		if(canAct()){
			if(energy!=100)
				normalAttack();
			else
				specialAttack(target);
		}
	}
	
	protected void normalAttack(){
		Sounds.getMisssound().play(NewSettings.getMastervol());
		stance = "atk";
		actionCooldown = atkDuration;
	}
	
	protected void specialAttack(Fighter target){
		Sounds.getProjsound().play(NewSettings.getMastervol());
		setEnergy(0);
		special.cast(target);
		stance = "atk";
		atkMissed = true;
		actionCooldown = atkDuration;
	}
	
	public void evade(){
		if (canAct()){
			stance = "eva";
			actionCooldown = evaDuration;
			invincible = true;
		}
	}
	
	public void moveRight(){
		if (canMove()){
			x += movement*Gdx.graphics.getDeltaTime();
			if (x>Constant.WINDOW_WIDTH-width)
				x=Constant.WINDOW_WIDTH-width;
		}
	}
	
	public void moveLeft(){
		if (canMove()){
			x -= movement*Gdx.graphics.getDeltaTime();
			if (x<0)
				x=0;
		}
	}
	
	public void dashRight(){
		if (stance.equals("standing") && canAct() && dashEnabled){
			Sounds.getDashsound().play(NewSettings.getMastervol());
			x += dashRange;
			if (x>Constant.WINDOW_WIDTH-width)
				x=Constant.WINDOW_WIDTH-width;
			setActionCooldown(dashRec);
			invincible = true;
		}
	}
	
	public void dashLeft(){
		if (stance.equals("standing") && canAct() && dashEnabled){
			Sounds.getDashsound().play(NewSettings.getMastervol());
			x -= dashRange;
			if (x<0) x=0;
			setActionCooldown(dashRec);
			invincible = true;
		}
	}
	
	public void recoveryUpdate(){
		actionCooldownUpdate();
		hurtStateUpdate();
		comboTimerUpdate();
		energyAutoCharge();
	}
	
	private void actionCooldownUpdate(){
		if (actionCooldown!=0){
			setActionCooldown(actionCooldown-1);
			if (actionCooldown==0){
				atkMissed = false;
				invincible = false;
				width = data[0];
				stance = "standing";
			}
		}
	}
	
	private void hurtStateUpdate(){
		if (recoveryHit!=0){
			setRecoveryHit(recoveryHit-1);
			if (recoveryHit<hitRec-10){
				BattleHud.setShake(false);
			}			
			if (recoveryHit==0){
				invincible = false;
			}
		}
	}
	
	private void energyAutoCharge(){
		if (energy<100 && !special.isActive() && canAct()){
			setEnergy(energy+energyChargeRate);
		}
	}
	
	private void comboTimerUpdate(){
		if (recoveryCombo!=0){
			setRecoveryCombo(recoveryCombo-1);
			if (recoveryCombo==0)
				combo = 0;
		}
	}
	
	public void hitCheck(Fighter target){
		if(targetHit(target)){
			hitSuccess(target);
			staminaUp();
		}else if(targetEvaded(target)){
			atkMissed = true;
		}
	}
	
	private void hitSuccess(Fighter target){
		target.takeDamage(atk, combo);
		addCombo();
	}
	
	public boolean targetHit(Fighter target){
		if(special.isActive() && !canAttackDuringSpecial)
			return false;
		return stance.equals("atk") && getHurtbox().overlaps(target)
			&& actionCooldown<atkDuration-atkActivationDelay
			&& actionCooldown>atkDuration-atkActivationDelay-atkActiveTime 
			&& !target.isInvincible() && !atkMissed && atk>0;
	}
	
	private boolean targetEvaded(Fighter target){
		return stance.equals("atk") && getHurtbox().overlaps(target) && target.getStance().equals("eva") && atk>0;
	}

	public void takeDamage(int dmg, int bonusDmg){
		BattleHud.setShake(true);
		Sounds.getHitsound().play(NewSettings.getMastervol());
		setHealth(getHealth()-(dmg+bonusDmg/3));
		setRecoveryHit(getHitRec());
		resetCombo();
	}

//===== GETTERS and SETTERS =====//
	
	public SpecialAttack getSpecialAttack(){
		return special;
	}
	
}
