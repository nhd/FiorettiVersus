package com.mygdx.game.fighters;

import java.sql.ResultSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.model.DatabaseManager;

public abstract class FighterRectangle extends Rectangle{
	
	protected String name;
	protected int slot;
	
	protected Rectangle hurtbox = new Rectangle();
	
	protected Texture sheet;
	protected TextureRegion[] sprites = new TextureRegion[6];
	protected int[] data = new int[4];
	
	protected int flip = 0;
	protected String position = "left";
	protected String stance = "standing";
	protected Vector2 v2 = new Vector2();
	
	public FighterRectangle(String name, int slot){		
		this.name = name;		
		this.slot = slot;
		x = 50;
		y = 5;
		retrieveTextureData();
	}
	
	private void retrieveTextureData(){
		data = DatabaseManager.retrieveTextureData(name);
	}
	
	protected void spriteSetup(){
		sheet = new Texture(Gdx.files.internal("fighters/"+name+".png"));
		sprites[0] = new TextureRegion(sheet,0,0,data[0],data[1]);
		sprites[1] = new TextureRegion(sheet,data[0],0,data[0]+data[2],data[1]);
		sprites[2] = new TextureRegion(sheet,data[0]*2+data[2],0,data[3],data[1]);
		sprites[3] = new TextureRegion(sheet,0,data[1],data[0],data[1]);
		sprites[4] = new TextureRegion(sheet,data[0],data[1],data[0]+data[2],data[1]);
		sprites[5] = new TextureRegion(sheet,data[0]*2+data[2],data[1],data[3],data[1]);
		width = data[0];
		height = data[1];
	}
	
	@Override
	public float getX(){
		float xpos = x;
		if (position.equals("right")){
			if (stance.equals("atk"))
				xpos -= data[2];
			else if (stance.equals("eva"))
				xpos -= data[3] - data[0];
		}
		return xpos;
	}
	
	@Override
	public float getY(){
		return y;		
	}	
	
	public Rectangle getHurtbox(){	
		hurtbox.y = y;
		hurtbox.width = data[2];
		hurtbox.height = data[1]/2;
		hurtbox.x = x;
		if (position.equals("left"))
			hurtbox.x += data[0];
		else
			hurtbox.x -= data[2];
		return hurtbox;
	}
	
	public boolean needFlip(Vector2 center){
		if(center.x<this.getCenter(v2).x){
			position = "right";
			return true;
		}else{
			position = "left";
			return false;
		}		
	}

	boolean currentFlip;
	public Sprite getSprite(Vector2 center){
		TextureRegion textureRegion = null;
		switch(stance){
		case "standing": 
			textureRegion = sprites[0];
			break;
		case "atk":
			textureRegion = sprites[1];
			break;
		case "eva":
			textureRegion = sprites[2];
			break;
		}
		Sprite sprite = new Sprite(textureRegion);
		if(stance.equals("standing")){
			sprite.flip(needFlip(center), false);
			currentFlip = needFlip(center);
		}else
			sprite.flip(currentFlip, false);
		return sprite;
	}	
	public int getSlot() {
		return slot;
	}

	public int[] getData() {
		return data;
	}
	
	public String getStance() {
		return stance;
	}

	public void setStance(String stance) {
		this.stance = stance;
	}
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	public String getName() {
		return name;
	}


}
