package com.mygdx.game.fighters;

import com.mygdx.game.specials.SarahSpecial;

public class Sarah extends Fighter{
	public Sarah(){
		super("sarah",7);
		spriteSetup();
		special = new SarahSpecial(this);
	}
}
