package com.mygdx.game.fighters;

import com.mygdx.model.Constant;
import com.mygdx.model.DatabaseManager;

public class FighterBattleData extends FighterRectangle {

	protected int health = Constant.DEFAULT_HP;
	protected int maxHealth = Constant.DEFAULT_MAX_HP;
	protected int movement = Constant.DEFAULT_MS;
	protected double energy = 0;
	
	protected int atk = Constant.DEFAULT_ATK;
	protected int dashRange = Constant.DEFAULT_DASH_RANGE;		
	protected double energyChargeRate = Constant.DEFAULT_STAM_REC_RATE;
	
	protected int atkDuration = Constant.DEFAULT_ATK_CD;
	protected int atkActivationDelay = 5;
	protected int atkActiveTime = 35;
	
	protected int evaDuration = Constant.DEFAULT_EVA_CD;
	
	protected int hitRec = Constant.DEFAULT_HIT_RECOVERY;
	protected int dashRec = Constant.DEFAULT_DASH_RECOVERY;

	protected int comboTimer = Constant.DEFAULT_COMBO_TIMER;
	protected int victory = 0;
	
	protected boolean canAttackDuringSpecial = false;
	
	protected float actionCooldown = 0;
	protected float recoveryHit = 0;
	protected float recoveryCombo = 0;
	
	protected boolean dashEnabled = true;
	protected boolean chainAtkEnabled = true;
	
	protected boolean invincible = false;
	protected boolean atkMissed = false;
	protected int combo = 0;
	
	protected int hea;
	protected int pow;
	protected int spd;
	protected int tec;
	protected int ene;
	
	public FighterBattleData(String name, int slot){
		super(name, slot);
		retrieveStats();
		applyStats();
	}

//=======================================
	
	private void applyStats(){
		maxHealth = Constant.DEFAULT_MAX_HP + 10*(hea-3);
		health = maxHealth;
		atk = Constant.DEFAULT_ATK + pow-3;
		movement = Constant.DEFAULT_MS + 75*(spd-3);
		dashRange = Constant.DEFAULT_DASH_RANGE + 20*(spd-3);
		dashRec = Constant.DEFAULT_DASH_RECOVERY - 5*(spd-3);
		energyChargeRate = Constant.DEFAULT_STAM_REC_RATE + 0.1*ene;
	}
	
	private void retrieveStats(){
		int[] stats = DatabaseManager.retrieveStats(name);
		hea = stats[0];
		pow = stats[1];
		spd = stats[2];
		tec = stats[3];
		ene = stats[4];
	}
	
	public void reset(int xpos){
		health = maxHealth;
		stance = "standing";
		x = xpos;
		actionCooldown = 0;
		recoveryHit = 0;
		invincible = false;
		resetCombo();
	}
	
	protected void staminaUp(){
		setEnergy(getEnergy()+10);
	}
	
	public void addCombo(){
		if (chainAtkEnabled){
			combo++;
			recoveryCombo = comboTimer;
		}
	}
	
	public void resetCombo(){
		if (chainAtkEnabled){
			combo = 0;
			recoveryCombo = 0;
		}
	}

//========================================	

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
		if (this.health<0) 
			this.health = 0;
		if (this.health>maxHealth)
			this.health = maxHealth;
	}

	public void setActionCooldown(float cooldown) {
		this.actionCooldown = cooldown;
		if (this.actionCooldown<0)
			this.actionCooldown = 0;
	}
	
	public float getRecoveryHit() {
		return recoveryHit;
	}

	public void setRecoveryHit(float recoveryHit) {
		invincible = true;
		this.recoveryHit = recoveryHit;
		if (this.recoveryHit<0) this.recoveryHit=0;
	}

	public boolean isInvincible() {
		return invincible;
	}

	public void setInvincible(boolean invincible) {
		this.invincible = invincible;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
		if (this.energy<0)
			this.energy = 0;
		else if (this.energy>100)
			this.energy = 100;
	}

	public boolean isMiss() {
		return atkMissed;
	}

	public int getCombo() {
		return combo;
	}

	public void setCombo(int combo) {
		this.combo = combo;
		if (this.combo<0)
			this.combo = 0;
	}

	public float getRecoveryCombo() {
		return recoveryCombo;
	}

	public void setRecoveryCombo(float recoveryCombo) {
		this.recoveryCombo = recoveryCombo;
	}

	public int getComboTimer() {
		return comboTimer;
	}

	public void setComboTimer(int comboTimer) {
		this.comboTimer = comboTimer;
	}

	public int getMaxHealth() {
		return maxHealth;
	}
	
	public void setMiss(boolean miss) {
		this.atkMissed = miss;
	}

	public int getHitRec() {
		return hitRec;
	}

	public void setHitRec(int hitRec) {
		this.hitRec = hitRec;
	}

	public int getVictory() {
		return victory;
	}

	public void setVictory(int victory) {
		this.victory = victory;
	}
	
	public void setMs(int ms) {
		this.movement = ms;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public void setDashRec(int dashRec) {
		this.dashRec = dashRec;
	}

	public void setStamRecRate(double stamRecRate) {
		this.energyChargeRate = stamRecRate;
	}

	public void setDashRange(int dashRange) {
		this.dashRange = dashRange;
	}

	public int getMovement() {
		return movement;
	}

	public void setMovement(int movement) {
		this.movement = movement;
	}

	public float getActionCooldown() {
		return actionCooldown;
	}

	public int getAtkDuration() {
		return atkDuration;
	}

	public void setAtkDuration(int atkDuration) {
		this.atkDuration = atkDuration;
	}

	public int getEvaDuration() {
		return evaDuration;
	}

	public void setEvaDuration(int evaDuration) {
		this.evaDuration = evaDuration;
	}

	public int getDashRec() {
		return dashRec;
	}
}
