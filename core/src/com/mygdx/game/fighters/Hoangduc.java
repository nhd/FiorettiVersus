package com.mygdx.game.fighters;

import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.HoangducSpecial;

public class Hoangduc extends Fighter{
	public Hoangduc(){
		super("hoang duc",10);
		spriteSetup();
		special = new HoangducSpecial(this);
		canAttackDuringSpecial = true;
	}
	
	protected void specialAttack(Fighter target){
		Sounds.getTransform().play(NewSettings.getMastervol());
		setEnergy(0);
		special.cast(target);
	}

}
