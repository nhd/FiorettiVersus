package com.mygdx.game.fighters;

import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.MarevaSpecial;

public class Mareva extends Fighter{
	public Mareva(){
		super("mareva",11);
		spriteSetup();
		special = new MarevaSpecial(this);
		canAttackDuringSpecial = true;
	}

	protected void specialAttack(Fighter target){
		setEnergy(0);
		special.cast(target);
	}

}
