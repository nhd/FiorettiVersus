package com.mygdx.game.fighters;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

public class CpuModule{

	private long timerStart;
	private long refreshInterval = 150;
	private float distance;
	private int movement = 0;
	private float dangerZone;
	private Vector2 vector = new Vector2();
	public static int aggressivity = 1;

	public void play(Fighter cpu, Fighter target) {
		dangerZone = target.getData()[0]/2 + target.getData()[2]+cpu.getData()[0]/2;
		distance = Math.abs(target.getCenter(vector).x - cpu.getCenter(vector).x);

		move(cpu);
		evade(cpu, target);
		if(timerStart==0){
			attack(cpu, target);			
			timerStart = TimeUtils.millis();
			rollMovement(cpu);
		}else{
			if(TimeUtils.millis()-timerStart>refreshInterval){
				timerStart = 0;
			}
		}
	}
	
	private void rollMovement(Fighter cpu){
		if(cpu.getPosition().equals("left"))
			movement = MathUtils.random(1+aggressivity,3+aggressivity);
		else
			movement = MathUtils.random(2-aggressivity,4-aggressivity);
	}
	
	private void attack(Fighter cpu, Fighter target){
		if(distance<=dangerZone || cpu.getEnergy()==100){
			cpu.attack(target);
		}
	}
	
	private void evade(Fighter cpu, Fighter target){
		if(target.targetHit(cpu) && MathUtils.random(1,2+aggressivity)==1)
			cpu.evade();
	}

	private void move(Fighter cpu) {	
		switch(movement){
		case 0: 				
			break;
		case 1: case 2:
			if(MathUtils.random(1,100)!=1)
				cpu.moveLeft();
			else
				cpu.dashLeft();
			break;
		case 3: case 4:
			if(MathUtils.random(1,100)!=1)
				cpu.moveRight();
			else
				cpu.dashRight();
			break;
		}
	}
	
	public static void toggleAggressivity(){
		aggressivity = 1 - aggressivity;
	}

}
