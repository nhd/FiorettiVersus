package com.mygdx.game.fighters;

import com.mygdx.game.specials.PaulSpecial;

public class Paul extends Fighter{
	public Paul(){
		super("paul",6);
		spriteSetup();
		special = new PaulSpecial(this);
	}
}
