
package com.mygdx.game.fighters;

import com.mygdx.game.specials.BaptisteSpecial;

public class Baptiste extends Fighter{
	public Baptiste(){
		super("baptiste",13);
		spriteSetup();
		special = new BaptisteSpecial(this);
	}
}
