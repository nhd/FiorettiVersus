package com.mygdx.game.fighters;

import com.mygdx.game.specials.BrunoSpecial;

public class Bruno extends Fighter{
	public Bruno(){
		super("bruno",14);
		spriteSetup();
		special = new BrunoSpecial(this);
	}
}
