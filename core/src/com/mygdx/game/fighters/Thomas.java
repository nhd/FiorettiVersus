package com.mygdx.game.fighters;

import com.mygdx.game.specials.ThomasSpecial;

public class Thomas extends Fighter{	
	public Thomas(){
		super("thomas",0);
		spriteSetup();
		special = new ThomasSpecial(this);
	}
	
}
