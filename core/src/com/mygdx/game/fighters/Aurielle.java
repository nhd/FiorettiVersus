package com.mygdx.game.fighters;

import com.mygdx.game.specials.AurielleSpecial;

public class Aurielle extends Fighter{
	public Aurielle(){
		super("aurielle",12);
		spriteSetup();
		special = new AurielleSpecial(this);
	}
	
}
