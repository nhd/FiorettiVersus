package com.mygdx.game.fighters;

import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.specials.MaddieSpecial;

public class Mariemadeleine extends Fighter{
	public Mariemadeleine(){
		super("maddie",4);
		spriteSetup();
		special = new MaddieSpecial(this);
		canAttackDuringSpecial = true;
	}

}
