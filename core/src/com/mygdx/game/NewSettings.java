package com.mygdx.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.mygdx.game.fighters.Aurielle;
import com.mygdx.game.fighters.Baptiste;
import com.mygdx.game.fighters.Bruno;
import com.mygdx.game.fighters.Camille;
import com.mygdx.game.fighters.Damiaan;
import com.mygdx.game.fighters.Fangshu;
import com.mygdx.game.fighters.Hoangduc;
import com.mygdx.game.fighters.Mareva;
import com.mygdx.game.fighters.Mariemadeleine;
import com.mygdx.game.fighters.Mytran;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.game.fighters.Paul;
import com.mygdx.game.fighters.Sarah;
import com.mygdx.game.fighters.Thomas;
import com.mygdx.game.fighters.Thuhien;
import com.mygdx.game.fighters.Timothee;

public class NewSettings {
	private static float mastervol = 0.25f;
	private int[] bpos = {174+3*74,256-8};
	private int[] rpos = {174,256+24-16};
	//right : x+    down : y-	
	
	private boolean stageReady = false; 
	private boolean randomStage = true;
	
	private int stage = 0;	
	private static int track = 0;
	
	public static int playoff = 0;
	public static int battle = 0;
	
	private int rule = 0;
	private int bestof = 1;
	private int firstto = 0;
	private int mode = 0;
	
	public static ArrayList<Integer> bracket8 = new ArrayList<>(8);
	public static ArrayList<Integer> bracket2 = new ArrayList<>(2);
	public static ArrayList<Integer> bracket4 = new ArrayList<>(4);
	public static int winner = -1;
	
	private float shakeIntensity = 4;
	
	private static final String[] playable = {"THOMAS","THU HIEN","TIMOTHEE","RANDOM","MARIE-MAD.","MY TRAN","PAUL","SARAH","DAMIAAN","FANG SHU","HOANG DUC","MAREVA","AURIELLE","BAPTISTE","BRUNO","CAMILLE"};
	public static final String[] modes = {"VERSUS","TOURNAMENT"};
	
	public static boolean isPlayable(String name){
		return (Arrays.asList(playable).contains(name));
	}
	
	public void resetVersusCursor(){
		bpos[0] = 396;
		bpos[1] = 232;
		rpos[0] = 174;
		rpos[1] = 264;
	}
	
	public void resetTournamentCursor(){
		bpos[0] = 396;
		bpos[1] = 64;
	}
	
	public Fighter getFighter(int slot){
		Fighter player = null;
		switch(slot){
		case 0:
			player = new Thomas();
			break;
		case 1:
			player = new Thuhien();
			break;
		case 2:
			player = new Timothee();
			break;
		case 4:
			player = new Mariemadeleine();
			break;
		case 5:
			player = new Mytran();
			break;
		case 6:
			player = new Paul();
			break;
		case 7:
			player = new Sarah();
			break;
		case 8:
			player = new Damiaan();
			break;
		case 9:
			player = new Fangshu();
			break;
		case 10:
			player = new Hoangduc();
			break;
		case 11:
			player = new Mareva();
			break;
		case 12:
			player = new Aurielle();
			break;
		case 13:
			player = new Baptiste();
			break;
		case 14:
			player = new Bruno();
			break;
		case 15:
			player = new Camille();
			break;
		}
		return player;
	}

	public void getRules(){
		switch(rule){
		case 0:
			bestof = 1;
			firstto = 0;
			break;
		case 1:
			bestof = 3;
			firstto = 0;
			break;
		case 2:
			bestof = 5;
			firstto = 0;
			break;
		case 3:
			bestof = 0;
			firstto = 3;
			break;
		case 4:
			bestof = 0;
			firstto = 5;
			break;
		}
	}	

	public void nextRule(){
		rule++;
		if (rule==5) rule = 0;
		getRules();
	}
	
	public void nextMode(){
		mode++;
		if (mode==modes.length) mode = 0;
	}		
	
	public int getMode() {
		return mode;
	}

	public void nextTrack(){
		track++;
		if (track==10) track = 0;
	}
	
	public int getBestof() {
		return bestof;
	}

	public int getFirstto() {
		return firstto;
	}

	public void previousTrack(){
		track--;
		if (track==-1) track = 9;
	}
	
	public void volumeUp(){
		mastervol += 0.01f;
		if (mastervol>1.0f) mastervol = 1.0f;
	}
	
	public void volumeDown(){
		mastervol -= 0.01;
		if (mastervol<=0.0f) mastervol = 0.0f;
	}

	public static float getMastervol() {
		return mastervol;
	}

	public static String getMastervolString() {
		int volume = (int)(mastervol*100);
		return Integer.toString(volume)+"%";
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public static int getTrack() {
		return track;
	}

	public boolean isStageReady() {
		return stageReady;
	}

	public void setStageReady(boolean stageReady) {
		this.stageReady = stageReady;
	}

	public boolean isRandomStage() {
		return randomStage;
	}

	public void setRandomStage(boolean randomStage) {
		this.randomStage = randomStage;
	}

	public float getShakeIntensity() {
		return shakeIntensity;
	}

	public void setShakeIntensity(float shakeIntensity) {
		this.shakeIntensity = shakeIntensity;
	}

	public int[] getRpos() {
		return rpos;
	}

	public int[] getBpos() {
		return bpos;
	}
	

	public void setRpos(int[] rpos) {
		this.rpos = rpos;
	}

	public void setBpos(int[] bpos) {
		this.bpos = bpos;
	}

	public int getRule() {
		return rule;
	}

	public static void resetBracket() {
		playoff=0;
		battle=0;
//		bracket8.clear();
		bracket4.clear();
		bracket2.clear();
		winner = -1;
	}
	
	public void setMode(int mode){
		this.mode = mode;
	}
	
}
