package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class SarahSpecial extends SpecialAttack{
	private Texture spriteL;
	private Texture spriteR;
	
	public SarahSpecial(Fighter user){
		super(user);
		setSprite();
		y = Constant.WINDOW_HEIGHT;
	}

	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = user.getX() + width*2;
				break;
			case "right":
				x = user.getX() - user.getData()[2]*2;
				break;
			}
			x = target.getX();
		}
	}
	
	public void move(){
		y -= speed*Gdx.graphics.getDeltaTime();
		if(y<-width)
			stop();
	}
	
	public void stop(){
		active = false;
		y = Constant.WINDOW_HEIGHT;
	}

	protected void specialEffect(){
		user.setHealth(user.getHealth()+Constant.DEFAULT_MAX_HP/5);
		Sounds.heal.play(NewSettings.getMastervol());
	}

}
