package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.fighters.Fighter;

public class MarevaSpecial extends SpecialAttack{
	public MarevaSpecial(Fighter user){
		super(user);
		setSprite();
		y = 0;
	}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			position = user.getPosition();
			switch(position){
			case "left":
				x = -width;
				break;
			case "right":
				x = 640;
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>640){
			active = false;
		}
	}
	
	public void stop(){
		active = false;
		x = -width;
	}
	
	public void hitCheck(Fighter target){
			if (this.overlaps(target) && !target.isInvincible()){
				target.takeDamage(power, user.getCombo());
				user.addCombo();
			}		
	}
}
