package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.screens.BattleHud;

public class ThomasSpecial extends SpecialAttack{
	private long finishTimer = -1;
	private Rectangle surimi1 = new Rectangle(51,13,78,242);
	private Rectangle surimi2 = new Rectangle(280,13,78,242);
	private Rectangle surimi3 = new Rectangle(509,13,78,242);
	
	public ThomasSpecial(Fighter user){
		super(user);
		setSprite();
		y = 720;
		x = 0;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
		}
	}
	
	public void move(){
		if(active){
			if(y<0){				
				BattleHud.setShake(true);
				y = 0;
				finishTimer = TimeUtils.millis();	
				Sounds.getBrusound().play(NewSettings.getMastervol());
			}else if(y>0){
				y -= speed*Gdx.graphics.getDeltaTime();
			}else{
				if(TimeUtils.millis()-finishTimer>=250){
					stop();
				}
			}
		}

	}
	
	public void stop(){
		BattleHud.setShake(false);
		y = 720;
		active = false;
	}
	
	public void hitCheck(Fighter target){
		if(y==0 && TimeUtils.millis()-finishTimer<=50){
			if(surimi1.overlaps(target)||surimi2.overlaps(target)||surimi3.overlaps(target)){
				super.hitCheck(target);
			}
		}

	}

}
