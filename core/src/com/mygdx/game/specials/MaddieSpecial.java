package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class MaddieSpecial extends SpecialAttack{
	
	private double start;
	private int min = 0;
	private int max = 4;
	private int duration = 6500;
	
	public MaddieSpecial(Fighter user){
		super(user);
		setSprite();
	}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			y = Constant.WINDOW_HEIGHT;
			respawn();
			start = TimeUtils.millis();
		}
	}
	
	private void respawn(){
		y=Constant.WINDOW_HEIGHT;
		min = 0;
		max = 2;
		if(target.getX()>Constant.WINDOW_WIDTH/2){
			min = 2;
			max = 4;
		}
		x = MathUtils.random(min,max)*128;
	}
	
	public void move(){
		y-=speed*Gdx.graphics.getDeltaTime();
		if(y<-height){			
			respawn();
		}
		if(TimeUtils.millis()-start>duration)
			stop();
	}
	
	public void stop(){
		y = Constant.WINDOW_HEIGHT;
		active = false;
	}
	
	public void hitCheck(Fighter target){
		if(this.overlaps(user)){
			user.setInvincible(true);
		}
		if(this.overlaps(target) && !target.isInvincible()){
			target.takeDamage(power, user.getCombo());
		}
	}
	
}
