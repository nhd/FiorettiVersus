package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;
import com.mygdx.screens.BattleHud;

public class BrunoSpecial extends SpecialAttack{
	private long finishTimer = -1;
	public BrunoSpecial(Fighter user){
		super(user);
		setSprite();
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			y = Constant.WINDOW_HEIGHT;
			switch(user.getPosition()){
			case "left":
				x = user.getX() + user.getData()[2];
				break;
			case "right":
				x = user.getX() - this.width;
				break;
			}
		}
	}
	
	public void move(){
		if(y<0){				
			BattleHud.setShake(true);
			y = 0;
			finishTimer = TimeUtils.millis();		
			Sounds.getBrusound().play(NewSettings.getMastervol());
		}else if(y>0){
			y -= speed*Gdx.graphics.getDeltaTime();
		}else{
			if(TimeUtils.millis()-finishTimer>=250){
				stop();
			}
		}
	}
	
	public void stop(){
		BattleHud.setShake(false);
		active = false;
		x = -width;
		y = Constant.WINDOW_HEIGHT;
	}
	
	public void hitCheck(Fighter target){
		if(y==0 && TimeUtils.millis()-finishTimer<=50){
			super.hitCheck(target);
		}
	}
}
