package com.mygdx.game.specials;

import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class HoangducSpecial extends SpecialAttack{
	
	private double start;
	private final int duration = 5000;
	
	public HoangducSpecial(Fighter user){
		super(user);
		setSprite();
		x=-width;
		y=0;
	}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			start = TimeUtils.millis();
			user.setDashRec(Constant.DEFAULT_DASH_RECOVERY/2);
			user.setMs((int)speed);
			user.setAtk(Constant.DEFAULT_ATK*power);
		}
	}
	
	public void move(){
		x=user.getX()-12;
		if(user.getStance().equals("atk") && user.getPosition().equals("left")){
			x=user.getX()+user.getData()[2]-25;
		}else if(user.getStance().equals("atk") && user.getPosition().equals("right")){
			x=user.getX();
		}
		if(TimeUtils.millis()-start>duration)
			stop();
	}
	
	public void stop(){
		target.setMs(Constant.DEFAULT_MS);
		user.setDashRec(Constant.DEFAULT_DASH_RECOVERY);
		user.setMs(Constant.DEFAULT_MS);
		active = false;
		target.setMs(Constant.DEFAULT_MS);
		user.setAtk(Constant.DEFAULT_ATK);
		x=-width;
	}
	
	public void hitCheck(Fighter target){}
	
}
