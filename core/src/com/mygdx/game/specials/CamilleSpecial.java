package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class CamilleSpecial extends SpecialAttack{
	// damage = amount of slow
	public CamilleSpecial(Fighter user){
		super(user);
		setSprite();
		y = 0;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		Sounds.theworld.play(NewSettings.getMastervol());
		this.target = target;
		if(!active){
			active = true;
			position = user.getPosition();
			debuff();
			switch(position){
			case "left":
				x = Constant.WINDOW_WIDTH-width;
				break;
			case "right":
				x = 0;
				break;
			}
		}
	}
	
	private void debuff(){
		target.setMs(target.getMovement()-300);
		target.setAtkDuration(target.getAtkDuration()+power);
		target.setDashRec(target.getDashRec()+power);
	}
	
	public void move(){
		
		switch(position){
		case "left":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>Constant.WINDOW_WIDTH){
			stop();
		}
	}
	
	public void stop(){
		active = false;
		x = 640;
		target.setMs(target.getMovement()+300);
		target.setAtkDuration(target.getAtkDuration()-power);
		target.setDashRec(target.getDashRec()-power);
	}
	
	public void hitCheck(Fighter target){}
	
}
