package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class ThuhienSpecial extends SpecialAttack{	
	public ThuhienSpecial(Fighter user){
		super(user);
		setSprite();
		y = 20;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = -width;
				break;
			case "right":
				x = Constant.WINDOW_WIDTH;
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>Constant.WINDOW_WIDTH){
			stop();
		}
	}
	
	public void stop(){
		active = false;
		hit = false;
	}

}
