package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class AurielleSpecial extends SpecialAttack{
	public AurielleSpecial(Fighter user){
		super(user);
		setSprite();
		y = 100;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = 360+width;
				break;
			case "right":
				x = -width;
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>Constant.WINDOW_WIDTH){
			active = false;
		}
	}
	
	public void stop(){
		active = false;
		x = 640;
	}

}
