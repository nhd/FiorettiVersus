package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.NewSettings;
import com.mygdx.game.Sounds;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class FangshuSpecial extends SpecialAttack{
	public FangshuSpecial(Fighter user){
		super(user);
		setSprite();
		y = 150;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		Sounds.getProjsound().play(NewSettings.getMastervol());
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = user.getX()+user.getData()[2];
				break;
			case "right":
				x = user.getX()-user.getData()[2];
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>Constant.WINDOW_WIDTH){
			stop();
		}
	}
	
	public void stop(){
		active = false;
	}
	
	protected void sfxHit(){
		Sounds.getExplosion().play(NewSettings.getMastervol()*1.25f);
	}
	
}
