package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.fighters.Fighter;

public class PaulSpecial extends SpecialAttack{	
	public PaulSpecial(Fighter user){
		super(user);
		super.setSprite();
	}

	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			y = 610;
			switch(position){
			case "left":
				x = user.getX() - width;
				break;
			case "right":
				x = user.getX() + user.getData()[2];
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x += speed*Gdx.graphics.getDeltaTime();
			y -= 1.75*speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x -= speed*Gdx.graphics.getDeltaTime();
			y -= 1.75*speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(y<-width/2)
			stop();
	}
	
	public void stop(){
		active = false;
		x = -width;
		y = 610;
	}

}
