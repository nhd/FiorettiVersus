package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;
import com.mygdx.model.DatabaseManager;

public abstract class SpecialAttack extends Rectangle {
	protected String position = "left";
	protected float speed;
	protected Fighter user = null;
	protected Fighter target = null;
	protected Sprite sprite;
	protected Sprite spriteFlip;
	protected boolean active = false;
	protected int power;
	protected boolean hit = false;
	
	public SpecialAttack(Fighter user){
		this.user = user;
		this.x = Constant.WINDOW_WIDTH;
		this.y = Constant.WINDOW_HEIGHT;
		int[] data = DatabaseManager.retrieveSpecialData(user.getName());
		this.power = data[0];
		this.speed = data[1];
	}

	public abstract void cast(Fighter target);
	protected abstract void move();
	public abstract void stop();
	
	protected void sfxHit(){}
	
	protected void hitCheck(Fighter target){
		if (this.overlaps(target) && !hit && !target.isInvincible() && !hit){
			target.takeDamage(power, user.getCombo());
			user.addCombo();
			hit = true;
			specialEffect();
			sfxHit();
		}else if(this.overlaps(target) && target.getStance().equals("eva")){
			hit = true;
		}		
	}
	
	protected void specialEffect(){};	

	protected void setSprite() {
		sprite = new Sprite(new Texture(Gdx.files.internal("specials/" + user.getName() + ".png")));
		spriteFlip = new Sprite(sprite);
		spriteFlip.flip(true, false);
		width = sprite.getWidth();
		height = sprite.getHeight();
	}

	public Sprite getSprite() {
		if(position.equals("right"))
			return spriteFlip;
		else
			return sprite;
	}

	public boolean isActive() {
		return active;
	}

	public void updateState() {
		if(isActive()){
			move();
			hitCheck(target);
		}
	}
	
	
}
