package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.fighters.Fighter;

public class MytranSpecial extends SpecialAttack{
	private int sign = 1;
	
	public MytranSpecial(Fighter user){
		super(user);
		setSprite();
		y = 100;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = -width;
				break;
			case "right":
				x = 640;
				break;
			}
		}
		sign = -2*MathUtils.random(0,1)+1;
	}
	
	int div = 75;
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			y = 100+100*MathUtils.sin(x/div)*sign;
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			y = 100+100*MathUtils.sin(x/div)*sign;
			break;
		}
		if(x<-width || x>640){
			active = false;
		}
	}
	
	public void stop(){
		active = false;
		x = -width;
	}
	
	protected void specialEffect(){
		target.setEnergy(target.getEnergy()/2);
	}

}
