package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class TimotheeSpecial extends SpecialAttack{
	
	private double start;
	private float sign;
	
	public TimotheeSpecial(Fighter user){
		super(user);
		setSprite();
		y = 125;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			start = TimeUtils.millis();
			position = user.getPosition();
			if(position.equals("right")){
				x=Constant.WINDOW_WIDTH-width;
				sign=-1;
			}else{
				x=0;
				sign=1;
			}
				
		}
	}
	
	public void move(){
		x+=sign*speed*Math.cos((TimeUtils.millis()-start)/750);
		if(TimeUtils.millis()-start>12000)
			stop();
	}
	
	public void stop(){
		active = false;
		x = -width;
	}
	
	public void hitCheck(Fighter target){
		if (this.overlaps(target) && !target.isInvincible()){
			target.takeDamage(power, user.getCombo());
			user.addCombo();
		}	
	}
	
}
