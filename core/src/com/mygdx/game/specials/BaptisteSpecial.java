package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.fighters.Fighter;

public class BaptisteSpecial extends SpecialAttack{
	public BaptisteSpecial(Fighter user){
		super(user);
		setSprite();
		y = 15;
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			hit = false;
			position = user.getPosition();
			switch(position){
			case "left":
				x = -width;
				break;
			case "right":
				x = 360+width;
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			break;
		}
		if(x<-width || x>640){
			active = false;
		}
	}
	
	public void stop(){
		active = false;
		x = 640;
	}

	protected void specialEffect(){
		if(target.getHealth()>user.getHealth()){
			int hpSwitch = user.getHealth();
			user.setHealth(target.getHealth());
			target.setHealth(hpSwitch);
		}
		if(target.getEnergy()>user.getEnergy()){
			double staminaSwitch = user.getEnergy();
			user.setEnergy(target.getEnergy());
			target.setEnergy(staminaSwitch);
		}
	}
}
