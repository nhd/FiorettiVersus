package com.mygdx.game.specials;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class DamiaanSpecial extends SpecialAttack{
	
	public DamiaanSpecial(Fighter user){
		super(user);
		setSprite();
	}
	
	public void setup(){}
	
	public void cast(Fighter target){
		this.target = target;
		if(!active){
			active = true;
			y = 300;
			x = -width;
			position = user.getPosition();
			switch(position){
			case "left":
				x = -width;
				break;
			case "right":
				x = Constant.WINDOW_WIDTH;
				break;
			}
		}
	}
	
	public void move(){
		switch(position){
		case "left":
			x+=speed*Gdx.graphics.getDeltaTime();
			y-=speed/3*Gdx.graphics.getDeltaTime();
			break;
		case "right":
			x-=speed*Gdx.graphics.getDeltaTime();
			y-=speed/3*Gdx.graphics.getDeltaTime();
			break;
		}		
		if(x>Constant.WINDOW_WIDTH || x<-width){
			stop();
		}
	}
	
	public void stop(){
		active = false;
		hit = false;
	}

}
