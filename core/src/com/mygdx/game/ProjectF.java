package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.controller.MasterInput;
import com.mygdx.model.AssetsManager;
import com.mygdx.model.DatabaseManager;
import com.mygdx.screens.StartupScreen;

public class ProjectF extends Game {
	private AssetsManager manager;
    public SpriteBatch batch;
    public BitmapFont font;
    private MasterInput inputs;
    private NewSettings settings;
    private BattleSettings battleSettings;   
    
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        inputs = new MasterInput(this);
        manager = new AssetsManager();
        settings = new NewSettings();
        battleSettings = new BattleSettings();
        setScreen(new StartupScreen(this));
    }

    public void render() {
        super.render();
    }
    
    public MasterInput getInputs(){
    	return this.inputs;
    }
    
    public NewSettings getSettings(){
    	return this.settings;
    }
    
    public BattleSettings getBattleSettings(){
    	return this.battleSettings;
    }
    
    public AssetManager getManager(){
    	return this.manager.manager;
    }
    
    public boolean isLoadingFinished(){
    	return this.manager.isLoadingFinished();
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
    }

}
