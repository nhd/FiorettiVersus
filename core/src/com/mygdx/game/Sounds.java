package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;

public class Sounds {
	
	private static Music op = Gdx.audio.newMusic(Gdx.files.internal("sounds/opening.ogg"));
	private static Music bgm = op;
	private static Sound select = Gdx.audio.newSound(Gdx.files.internal("sounds/menuselect.wav"));
	private static Sound cancel = Gdx.audio.newSound(Gdx.files.internal("sounds/menucancel.wav"));
	private static Sound beep = Gdx.audio.newSound(Gdx.files.internal("sounds/menubeep.wav"));
	
	private static Sound hitsound = Gdx.audio.newSound(Gdx.files.internal("sounds/Blow8.ogg"));
	private static Sound evasound = Gdx.audio.newSound(Gdx.files.internal("sounds/evade.ogg"));
	private static Sound dashsound = Gdx.audio.newSound(Gdx.files.internal("sounds/teleport.ogg"));
	private static Sound misssound = Gdx.audio.newSound(Gdx.files.internal("sounds/miss.ogg"));
	private static Sound transform = Gdx.audio.newSound(Gdx.files.internal("sounds/aura.ogg"));
//	
	private static Sound readysound = Gdx.audio.newSound(Gdx.files.internal("sounds/ready.ogg"));
	private static Sound gosound = Gdx.audio.newSound(Gdx.files.internal("sounds/go.ogg"));
	private static Sound endsound = Gdx.audio.newSound(Gdx.files.internal("sounds/end.ogg"));
	private static Sound projsound = Gdx.audio.newSound(Gdx.files.internal("sounds/Skill3.ogg"));
	
	private static Sound bracket = Gdx.audio.newSound(Gdx.files.internal("sounds/bracket.ogg"));
	
	private static Sound brusound = Gdx.audio.newSound(Gdx.files.internal("sounds/bruspe.ogg"));
	
	private static Sound explosion = Gdx.audio.newSound(Gdx.files.internal("sounds/explosion.ogg"));
	public static Sound theworld = Gdx.audio.newSound(Gdx.files.internal("sounds/timeslow.ogg"));
	public static Sound heal = Gdx.audio.newSound(Gdx.files.internal("sounds/heal.ogg"));
	public static Sound champion = Gdx.audio.newSound(Gdx.files.internal("sounds/champion.ogg"));
	public static Music finalRound = Gdx.audio.newMusic(Gdx.files.internal("sounds/finalRound.ogg"));
	
	public static Music bgmSelector(){
		int music = NewSettings.getTrack();
		if (music==0)
			music = MathUtils.random(1,9);
		switch(music){
		case 1:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/gintoki.ogg"));
			break;
		case 2:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/goku.ogg"));
			break;
		case 3:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/ichigo.ogg"));
			break;
		case 4:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/kenshiro.ogg"));
			break;
		case 5:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/koro.ogg"));
			break;
		case 6:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/luffy.ogg"));
			break;
		case 7:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/naruto.ogg"));
			break;
		case 8:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/seiya.ogg"));
			break;
		case 9:
			bgm = Gdx.audio.newMusic(Gdx.files.internal("sounds/yusuke.ogg"));
			break;
		}		
		return bgm;
	}

	public static Music getBgm() {
		getOp().setVolume(NewSettings.getMastervol());
		return bgm;
	}
	
	public static Music getOp(){
		return op;
	}

	public static Sound getSelect() {
		return select;
	}

	public static Sound getCancel() {
		return cancel;
	}

	public static Sound getBeep() {
		return beep;
	}

	public static Sound getHitsound() {
		return hitsound;
	}

	public static Sound getEvasound() {
		return evasound;
	}

	public static Sound getDashsound() {
		return dashsound;
	}

	public static Sound getMisssound() {
		return misssound;
	}

	public static Sound getReadysound() {
		return readysound;
	}

	public static Sound getGosound() {
		return gosound;
	}

	public static Sound getEndsound() {
		return endsound;
	}

	public static Sound getProjsound() {
		return projsound;
	}

	public static Sound getBrusound() {
		return brusound;
	}

	public static Sound getBracket() {
		return bracket;
	}

	public static Sound getTransform() {
		return transform;
	}

	public static Sound getExplosion() {
		return explosion;
	}
	
}
