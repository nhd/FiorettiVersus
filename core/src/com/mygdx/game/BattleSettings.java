package com.mygdx.game;

import com.mygdx.game.fighters.Fighter;
import com.mygdx.model.Constant;

public class BattleSettings {
	private boolean vsCpu = false;
	private boolean stageReady = false; 
	private boolean randomStage = true;

	private int battleStage = 0;	

	private int playoff = 0;
	private int battle = 0;

	private int battleRule = 0;
	private int gameMode = 0;
	
	private Fighter bluePlayer;
	private Fighter redPlayer;
	private int blueSlot;
	private int redSlot;

//==========METHODS
	
	public void toggeVsCpu(){
		vsCpu = !vsCpu;
	}
	
	public int getMode() {
		return gameMode;
	}
	
	public void nextRule(){
		battleRule++;
		if (battleRule==Constant.NUMBER_OF_RULES)
			battleRule = 0;
	}
	
	public int getGameMode(){
		return gameMode;
	}
	
	public void setTournamentMode(){
		gameMode = Constant.TOURNAMENT_MODE;
		vsCpu = false;
	}
	
	public void setVersusMode(){
		gameMode= Constant.VERSUS_MODE;
	}

	public Fighter getBluePlayer() {
		return bluePlayer;
	}

	public void setBluePlayer(Fighter bluePlayer) {
		this.bluePlayer = bluePlayer;
	}

	public Fighter getRedPlayer() {
		return redPlayer;
	}

	public void setRedPlayer(Fighter redPlayer) {
		this.redPlayer = redPlayer;
	}

	public void saveFightersSlot(int blueSlot, int redSlot) {
		this.blueSlot = blueSlot;
		this.redSlot = redSlot;
	}

	public void setupFighters() {
		bluePlayer = Constant.getFighter(blueSlot);
		redPlayer = Constant.getFighter(redSlot);
	}
	
	public boolean isVsCpu(){
		return vsCpu;
	}
	
	public void toggleVsCpu(){
		vsCpu = !vsCpu;
	}
	
	
	
}
