package com.mygdx.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.badlogic.gdx.utils.TimeUtils;


public class DatabaseManager {

	private static final String url = "jdbc:sqlite::resource:fighters.sqlite";
	
    private static Connection connect() {
       Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);    
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public static boolean firstTimeConnect(){
    	connect();
    	return true;
    }

    public static int[] retrieveTextureData(String name){
    	int[] result = new int[4];
    	String sql = "SELECT width, height, range, widthEvasion FROM texture WHERE name = '"+name+"'";
    	try{
    		Connection conn = connect();
    		Statement statement = conn.createStatement();
    		ResultSet res = statement.executeQuery(sql);
    		while(res.next()){
    			result[0] = res.getInt("width");
    			result[1] = res.getInt("height");
    			result[2] = res.getInt("range");
    			result[3] = res.getInt("widthEvasion");
    		}
    		statement.close();
    		res.close();    	
    		conn.close();
    	}catch(SQLException e){
    		e.printStackTrace();
    	}
    	return result;
    }    
    
    public static int[] retrieveStats(String name){
    	int[] result = new int[5];
    	String sql = "SELECT health, power, speed, technique, energy FROM stats WHERE name = '"+name+"'";
    	try{
    		Connection conn = connect();
    		Statement statement = conn.createStatement();
    		ResultSet res = statement.executeQuery(sql);
    		while(res.next()){
    			result[0] = res.getInt("health");
    			result[1] = res.getInt("power");
    			result[2] = res.getInt("speed");
    			result[3] = res.getInt("technique");
    			result[4] = res.getInt("energy");
    		}
    		statement.close();
    		res.close();    	
    		conn.close();
    	}catch(SQLException e){
    		e.printStackTrace();
    	}
    	return result;
    }    
    
    public static int[] retrieveSpecialData(String name){
    	int[] result = new int[2];
    	String sql = "SELECT power, speed FROM special WHERE name = '"+name+"'";
    	try{
    		Connection conn = connect();
    		Statement statement = conn.createStatement();
    		ResultSet res = statement.executeQuery(sql);
    		while(res.next()){
    			result[0] = res.getInt("power");
    			result[1] = res.getInt("speed");
    		}
    		statement.close();
    		res.close();    	
    		conn.close();
    	}catch(SQLException e){
    		e.printStackTrace();
    	}
    	return result;
    }   
	
}
