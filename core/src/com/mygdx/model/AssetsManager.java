package com.mygdx.model;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

public class AssetsManager {
	public AssetManager manager;
	
	public AssetsManager() {
		manager = new AssetManager();
		loadAll();
	}
	
	public void loadAll(){
		loadStageSelectAsset();
		loadBattleHud();
		loadCharSelectAsset();
		loadBracketAsset();
		loadSettingsAsset();
		loadTitleAsset();
	}
	
	private void loadStageSelectAsset(){
		manager.load(AssetConstant.STAGE_SEL_MUSIC_ICON, Texture.class);
		manager.load(AssetConstant.STAGE_SEL_STAGES, Texture.class);
		manager.load(AssetConstant.STAGE_SEL_BG_MUSIC, Music.class);	
		manager.load(AssetConstant.STAGE_TOURNAMENT, Texture.class);
	}
	
	private void loadCharSelectAsset(){
		manager.load(AssetConstant.CHAR_SEL_BG, Texture.class);
		manager.load(AssetConstant.CHAR_SEL_BG_MUSIC, Music.class);
		manager.load(AssetConstant.CHAR_SEL_PORTRAITS, Texture.class);
		manager.load(AssetConstant.CHAR_SEL_ICONS, Texture.class);
		manager.load(AssetConstant.CHAR_SEL_CURSORS, Texture.class);
	}
	
	private void loadBracketAsset(){
		manager.load(AssetConstant.CHAR_SEL_TOURNAMENT, Texture.class);
		manager.load(AssetConstant.BRACKET_BG, Texture.class);
		manager.load(AssetConstant.BRACKET_CROSS, Texture.class);
	}
	
	private void loadSettingsAsset(){
		manager.load(AssetConstant.SETTINGS_BG, Texture.class);
		manager.load(AssetConstant.SETTINGS_CURSOR, Texture.class);
	}
	
	private void loadTitleAsset(){
		manager.load(AssetConstant.TITLE_BG, Texture.class);
		manager.load(AssetConstant.TITLE_CURSOR, Texture.class);
	}
	
	private void loadBattleHud(){
		manager.load(AssetConstant.BATTLE_HUD, Texture.class);		
	}
	
	public boolean isLoadingFinished(){
		return manager.update();
	}
}
