package com.mygdx.model;

import com.mygdx.game.fighters.Aurielle;
import com.mygdx.game.fighters.Baptiste;
import com.mygdx.game.fighters.Bruno;
import com.mygdx.game.fighters.Camille;
import com.mygdx.game.fighters.Damiaan;
import com.mygdx.game.fighters.Fangshu;
import com.mygdx.game.fighters.Hoangduc;
import com.mygdx.game.fighters.Mareva;
import com.mygdx.game.fighters.Mariemadeleine;
import com.mygdx.game.fighters.Mytran;
import com.mygdx.game.fighters.Fighter;
import com.mygdx.game.fighters.Paul;
import com.mygdx.game.fighters.Sarah;
import com.mygdx.game.fighters.Thomas;
import com.mygdx.game.fighters.Thuhien;
import com.mygdx.game.fighters.Timothee;

public class Constant {
	public static String GAME_NAME = "Fioretti Versus";
	public static final float WINDOW_WIDTH = 640.0f;
	public static final float WINDOW_HEIGHT = 360.0f;
	public static final String ICON_PATH = "graphics/icon.png";
	public static final String GAME_VERSION = "0.9.0";
	
	public static final String[] NAME = {"THOMAS","THU HIEN","TIMOTHEE","RANDOM","MARIE-MAD.","MY TRAN","PAUL","SARAH","DAMIAAN","FANG SHU","HOANG DUC","MAREVA","AURIELLE","BAPTISTE","BRUNO","CAMILLE"};
	public static final String[] TITLE_OPTION = {"SETTINGS","TOURNAMENT","BATTLE"};
	public static final String[] BATTLE_RULE = {"BEST OF 1", "BEST OF 3", "BEST OF 5", "FIRST TO 3", "FIRST TO 5"};
	
	public static final int NUMBER_OF_RULES = 5;
	public static final int TOURNAMENT_MODE = 1;
	public static final int VERSUS_MODE = 0;
	
// TITLE SCREEN	
	public static final int TITLE_CURSOR_MOVE = 25;
	public static final int BATTLE_SELECT = 2;
	public static final int TOURNAMENT_SELECT = 1;
	public static final int SETTINGS_SELECT = 0;
	public static final int OPTIONS_Y = 15;
	
// CHAR SEELCT
	public static final int CURSOR_MOVE_Y = 56;
	public static final int CURSOR_MOVE_X = 74;
	
	public static final int CURSOR_MIN_X = 174;
	public static final int CURSOR_MIN_Y = 64;
	public static final int CURSOR_MAX_X = 396;
	public static final int CURSOR_MAX_Y = 232;
	
	public static final int CHAR_GRID_ROW = 4;
	public static final int CHAR_GRID_COL = 4;
	public static final int RAND_CHAR_SLOT = 3;
	public static final int NUMBER_OF_CHAR = 15;
	
	public static final int NO_ACTION = 0;
	public static final int DISPOSE_ACTION = 1;
	
// PLAYER
	public static final int STANDING = 0;
	public static final int ATTACKING = 1;
	public static final int EVADING = 2;
	
	public static final int DEFAULT_HP = 100;
	public static final int DEFAULT_MAX_HP = 100;
	public static final int DEFAULT_MS = 500;
	public static final int DEFAULT_ATK = 5;
	public static final int DEFAULT_DASH_RANGE = 150;
	public static final double DEFAULT_STAM_REC_RATE = 0.25;
	public static final int DEFAULT_ATK_CD = 40;
	public static final int DEFAULT_EVA_CD = 30;
	public static final int DEFAULT_HIT_RECOVERY = 40;
	public static final int DEFAULT_DASH_RECOVERY = 20;
	public static final int DEFAULT_COMBO_TIMER = 200;
	
	public static Fighter getFighter(int slot){
		Fighter player = null;
		switch(slot){
		case 0:
			player = new Thomas();
			break;
		case 1:
			player = new Thuhien();
			break;
		case 2:
			player = new Timothee();
			break;
		case 4:
			player = new Mariemadeleine();
			break;
		case 5:
			player = new Mytran();
			break;
		case 6:
			player = new Paul();
			break;
		case 7:
			player = new Sarah();
			break;
		case 8:
			player = new Damiaan();
			break;
		case 9:
			player = new Fangshu();
			break;
		case 10:
			player = new Hoangduc();
			break;
		case 11:
			player = new Mareva();
			break;
		case 12:
			player = new Aurielle();
			break;
		case 13:
			player = new Baptiste();
			break;
		case 14:
			player = new Bruno();
			break;
		case 15:
			player = new Camille();
			break;
		}
		return player;
	}
}
