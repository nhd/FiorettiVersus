package com.mygdx.model;

public class AssetConstant {
	private static final String GRAPHICS_PATH = "graphics/";
	private static final String SOUNDS_PATH = "sounds/";
	
	private static final String IMG_FORMAT = ".png";
	private static final String SOUND_FORMAT = ".ogg";
	
	public static final String STAGE_SEL_STAGES = GRAPHICS_PATH + "stages" + IMG_FORMAT;
	public static final String STAGE_SEL_MUSIC_ICON = GRAPHICS_PATH + "music" + IMG_FORMAT;
	public static final String STAGE_SEL_BG_MUSIC = SOUNDS_PATH + "stagesel" + SOUND_FORMAT;
	
	public static final String BATTLE_HUD = GRAPHICS_PATH + "hud" + IMG_FORMAT;
	public static final String STAGE_TOURNAMENT = GRAPHICS_PATH + "tournament_stage" + IMG_FORMAT;
	
	public static final String CHAR_SEL_BG = GRAPHICS_PATH + "background" + IMG_FORMAT;
	public static final String CHAR_SEL_BG_MUSIC = SOUNDS_PATH + "charsel" + SOUND_FORMAT;
	public static final String CHAR_SEL_PORTRAITS = GRAPHICS_PATH + "portraits" + IMG_FORMAT;
	public static final String CHAR_SEL_ICONS = GRAPHICS_PATH + "icons" + IMG_FORMAT;
	public static final String CHAR_SEL_CURSORS = GRAPHICS_PATH + "cursor" + IMG_FORMAT;
	
	public static final String CHAR_SEL_TOURNAMENT = GRAPHICS_PATH + "background_tournament" + IMG_FORMAT;
	public static final String BRACKET_BG = GRAPHICS_PATH + "tournamentBracket" + IMG_FORMAT;
	public static final String BRACKET_CROSS = GRAPHICS_PATH + "redCross" + IMG_FORMAT;
	
	public static final String SETTINGS_BG = GRAPHICS_PATH + "settings" + IMG_FORMAT;
	public static final String SETTINGS_CURSOR = GRAPHICS_PATH + "cursorSettings" + IMG_FORMAT;
	
	public static final String TITLE_BG = GRAPHICS_PATH + "titleScreen" + IMG_FORMAT;
	public static final String TITLE_CURSOR = GRAPHICS_PATH + "cursorTitle" + IMG_FORMAT;
	
}
