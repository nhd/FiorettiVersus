package com.mygdx.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.ProjectF;
import com.mygdx.model.Constant;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int)Constant.WINDOW_WIDTH;
		config.height = (int)Constant.WINDOW_HEIGHT;
		config.title = Constant.GAME_NAME;
		config.addIcon(Constant.ICON_PATH, Files.FileType.Internal);
		new LwjglApplication(new ProjectF(), config);
	}
}
